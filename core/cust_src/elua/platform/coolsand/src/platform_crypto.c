/**************************************************************************
 *              Copyright (C), AirM2M Tech. Co., Ltd.
 *
 * Name:    platform_fs.c
 * Author:  liweiqiang
 * Version: V0.1
 * Date:    2012/11/27
 *
 * Description:
 * 
 **************************************************************************/

#include "rda_pal.h"

VOID platform_crypto_sha256(UINT8 *pData, INT32 nLen, UINT8 *pDigest)
{
    AMOPENAT_SHA256_CTX ctx;
    memset(&ctx, 0, sizeof(ctx));   
  
    IVTBL(sha256_init)(&ctx);
    IVTBL(sha256_update)(&ctx, pData, nLen);
    IVTBL(sha256_final)(&ctx, pDigest);
}


