#ifdef AM_CAMERA_SUPPORT
#include "string.h"
#include "malloc.h"

#include "rda_pal.h"
#include "cycle_queue.h"
#include "assert.h"

#include "platform.h"
#include "platform_conf.h"
#include "platform_rtos.h"
#include "platform_camera.h"

//#define CAMERA_MIRROR

#define CAMERA_PREVIEW_WITHE 640
#define CAMERA_PREVIEW_HEIGHT 480

unsigned char *g_scanner_buff = NULL;//[CAMERA_PREVIEW_WITHE*CAMERA_PREVIEW_HEIGHT/4];
//unsigned char g_scanner_buff[CAMERA_PREVIEW_WITHE*CAMERA_PREVIEW_HEIGHT/4];
#define ZBAR_IMAGE_START_X 0
#define ZBAR_IMAGE_START_Y 0
#define ZBAR_IMAGE_END_X (CAMERA_PREVIEW_WITHE/2)
#define ZBAR_IMAGE_END_Y (CAMERA_PREVIEW_HEIGHT/2)


#define CAM_BY3A01_ID   0x01

#define CAMERA_I2C_ADDR (0X21)
#define CAM_GC0310   0x10
#define CAM_ID_REG 0xf1

static BOOL sMirror = FALSE;

static AMOPENAT_CAMERA_REG cameraInitReg_gc0310 [] = {
  {0xfe,0xf0},
	{0xfe,0xf0},
	{0xfe,0x00},

	{0xfc,0x16}, //4e 
	{0xfc,0x16}, //4e // [0]apwd [6]regf_clk_gate 
	{0xf2,0x07}, //sync output
	{0xf3,0x83}, //ff//1f//01 data output
	{0xf5,0x07}, //sck_dely

	{0xf7,0x88}, //f8/   88
	{0xf8,0x00},           // 00
	{0xf9,0x4f}, //0f//01   4d
	{0xfa,0x32}, //32

	{0xfc,0xce},
	{0xfd,0x00},
	/////////////////////////////////////////////////
	/////////////////   CISCTL reg  /////////////////
	/////////////////////////////////////////////////
#if 1//def DSS_AIR202_LUA_CAMERA
	{0x00,0x2f}, 
	{0x01,0x0f}, 
	{0x02,0x04},

	{0x03,0x02},
	{0x04,0x12},

	{0x09,0x00}, 
	{0x0a,0x00}, 
	{0x0b,0x00}, 
	{0x0c,0x02},//04 
	{0x0d,0x01}, 
	{0x0e,0xec},//e8 
	{0x0f,0x02}, 
	{0x10,0x88}, 
	{0x16,0x00},
#if 0//def CAMERA_MIRROR
	{0x17,0x15},
#else
	{0x17,0x14},
#endif
	{0x18,0x6a},//1a 
	{0x19,0x14}, 
	{0x1b,0x48},
	{0x1c,0x1c},
	{0x1e,0x6b},
	{0x1f,0x28},
	{0x20,0x8b},//0x89 travis20140801
	{0x21,0x49},
	{0x22,0xb0},
	{0x23,0x04}, 
	{0x24,0xff}, 
	{0x34,0x20}, 


	/////////////////////////////////////////////////
	////////////////////   BLK   ////////////////////
	/////////////////////////////////////////////////
	{0x26,0x23},
	{0x28,0xff},
	{0x29,0x00},
	{0x32,0x00},
	{0x33,0x10}, 
	{0x37,0x20},
	{0x38,0x10},
	{0x47,0x80},
	{0x4e,0x0f},//66
	{0xa8,0x02},
	{0xa9,0x80},


	/////////////////////////////////////////////////
	//////////////////   ISP reg  ///////////////////
	/////////////////////////////////////////////////
	{0x40,0xff},
	{0x41,0x21},
	{0x42,0xcf},
	{0x44,0x02},
	{0x45,0xa8}, 
	{0x46,0x02}, //sync
	{0x4a,0x11},
	{0x4b,0x01},
	{0x4c,0x20},
	{0x4d,0x05},
	{0x4f,0x01},
	{0x50,0x01},
	{0x55,0x00},
	{0x56,0xf0},
	{0x57,0x01},
	{0x58,0x40},
#else
		{0x00,0x2f}, 
	{0x01,0x0f}, 
	{0x02,0x04},

	{0x03,0x02},
	{0x04,0x12},

	{0x09,0x00}, 
	{0x0a,0x00}, 
	{0x0b,0x00}, 
	{0x0c,0x04}, 
	{0x0d,0x01}, 
	{0x0e,0xe8}, 
	{0x0f,0x02}, 
	{0x10,0x88}, 
	{0x16,0x00},
	{0x17,0x14},
	{0x18,0x1a}, 
	{0x19,0x14}, 
	{0x1b,0x48},
	{0x1c,0x1c},
	{0x1e,0x6b},
	{0x1f,0x28},
	{0x20,0x8b},//0x89 travis20140801
	{0x21,0x49},
	{0x22,0xb0},
	{0x23,0x04}, 
	{0x24,0xff}, 
	{0x34,0x20}, 


	/////////////////////////////////////////////////
	////////////////////   BLK   ////////////////////
	/////////////////////////////////////////////////
	{0x26,0x23},
	{0x28,0xff},
	{0x29,0x00},
	{0x32,0x00},
	{0x33,0x10}, 
	{0x37,0x20},
	{0x38,0x10},
	{0x47,0x80},
	{0x4e,0x66},
	{0xa8,0x02},
	{0xa9,0x80},


	/////////////////////////////////////////////////
	//////////////////   ISP reg  ///////////////////
	/////////////////////////////////////////////////
	{0x40,0xff},
	{0x41,0x21},
	{0x42,0xcf},
	{0x44,0x02},
	{0x45,0xa8}, 
	{0x46,0x02}, //sync
	{0x4a,0x11},
	{0x4b,0x01},
	{0x4c,0x20},
	{0x4d,0x05},
	{0x4f,0x01},
	{0x50,0x01},
	{0x55,0x01},
	{0x56,0xe0},
	{0x57,0x02},
	{0x58,0x80},
#endif
	/////////////////////////////////////////////////
	///////////////////   GAIN   ////////////////////
	/////////////////////////////////////////////////
	{0x70,0x70},
	{0x5a,0x84},
	{0x5b,0xc9},
	{0x5c,0xed},
	{0x77,0x74},
	{0x78,0x40},
	{0x79,0x5f}, 


	///////////////////////////////////////////////// 
	///////////////////   DNDD  /////////////////////
	///////////////////////////////////////////////// 
	{0x82,0x08},//0x14 
	{0x83,0x0b},
	{0x89,0xf0},
	///////////////////////////////////////////////// 
	//////////////////   EEINTP  ////////////////////
	///////////////////////////////////////////////// 
	{0x8f,0xaa},
	{0x90,0x8c},
	{0x91,0x90},
	{0x92,0x03},
	{0x93,0x03},
	{0x94,0x05},
	{0x95,0x43}, //0x65
	{0x96,0xf0}, 
	///////////////////////////////////////////////// 
	/////////////////////  ASDE  ////////////////////
	///////////////////////////////////////////////// 
	{0xfe,0x00},

	{0x9a,0x20},
	{0x9b,0x80},
	{0x9c,0x40},
	{0x9d,0x80},
	 
	{0xa1,0x30},
 	{0xa2,0x32},
	{0xa4,0x30},
	{0xa5,0x30},
	{0xaa,0x10}, 
	{0xac,0x22},
	 

	/////////////////////////////////////////////////
	///////////////////   GAMMA   ///////////////////
	/////////////////////////////////////////////////
	{0xfe,0x00},//default
	{0xbf,0x08},
	{0xc0,0x16},
	{0xc1,0x28},
	{0xc2,0x41},
	{0xc3,0x5a},
	{0xc4,0x6c},
	{0xc5,0x7a},
	{0xc6,0x96},
	{0xc7,0xac},
	{0xc8,0xbc},
	{0xc9,0xc9},
	{0xca,0xd3},
	{0xcb,0xdd},
	{0xcc,0xe5},
	{0xcd,0xf1},
	{0xce,0xfa},
	{0xcf,0xff},
	
/* 
	{0xfe,0x00},//big gamma
	{0xbf,0x08},
	{0xc0,0x1d},
	{0xc1,0x34},
	{0xc2,0x4b},
	{0xc3,0x60},
	{0xc4,0x73},
	{0xc5,0x85},
	{0xc6,0x9f},
	{0xc7,0xb5},
	{0xc8,0xc7},
	{0xc9,0xd5},
	{0xca,0xe0},
	{0xcb,0xe7},
	{0xcc,0xec},
	{0xcd,0xf4},
	{0xce,0xfa},
	{0xcf,0xff},
*/	

/*
	{0xfe,0x00},//small gamma
	{0xbf,0x08},
	{0xc0,0x18},
	{0xc1,0x2c},
	{0xc2,0x41},
	{0xc3,0x59},
	{0xc4,0x6e},
	{0xc5,0x81},
	{0xc6,0x9f},
	{0xc7,0xb5},
	{0xc8,0xc7},
	{0xc9,0xd5},
	{0xca,0xe0},
	{0xcb,0xe7},
	{0xcc,0xec},
	{0xcd,0xf4},
	{0xce,0xfa},
	{0xcf,0xff},
*/
	/////////////////////////////////////////////////
	///////////////////   YCP  //////////////////////
	/////////////////////////////////////////////////
	{0xd0,0x40}, 
	{0xd1,0x38}, //0x34
	{0xd2,0x38}, //0x34
	{0xd3,0x50},//0x40 
	{0xd6,0xf2}, 
	{0xd7,0x1b}, 
	{0xd8,0x18}, 
	{0xdd,0x03}, 

	/////////////////////////////////////////////////
	////////////////////   AEC   ////////////////////
	/////////////////////////////////////////////////
	{0xfe,0x01},
	{0x05,0x30},
	{0x06,0x75},
	{0x07,0x40},
	{0x08,0xb0},
	{0x0a,0xc5},
	{0x0b,0x11},
	{0x0c,0x00},
	{0x12,0x52},
	{0x13,0x38},
	{0x18,0x95},
	{0x19,0x96},
	{0x1f,0x20}, 
	{0x20,0xc0}, 
	{0x3e,0x40}, 
	{0x3f,0x57}, 
	{0x40,0x7d}, 

	{0x03,0x60}, 

	{0x44,0x02}, 
	/////////////////////////////////////////////////
	////////////////////   AWB   ////////////////////
	/////////////////////////////////////////////////
	{0xfe,0x01},
	{0x1c,0x91}, 
	{0x21,0x15}, 
	{0x50,0x80},
	{0x56,0x04},
	{0x59,0x08}, 
	{0x5b,0x02},
	{0x61,0x8d}, 
	{0x62,0xa7}, 
	{0x63,0xd0}, 
	{0x65,0x06},
	{0x66,0x06}, 
	{0x67,0x84}, 
	{0x69,0x08}, 
	{0x6a,0x25}, 
	{0x6b,0x01}, 
	{0x6c,0x00}, 
	{0x6d,0x02}, 
	{0x6e,0xf0}, 
	{0x6f,0x80}, 
	{0x76,0x80}, 
	{0x78,0xaf}, 
	{0x79,0x75},
	{0x7a,0x40},
	{0x7b,0x50},
	{0x7c,0x0c}, 

	{0x90,0xc9},//stable AWB 
	{0x91,0xbe},
	{0x92,0xe2},
	{0x93,0xc9},
	{0x95,0x1b},
	{0x96,0xe2},
	{0x97,0x49},
	{0x98,0x1b},
	{0x9a,0x49},
	{0x9b,0x1b},
	{0x9c,0xc3},
	{0x9d,0x49},
	{0x9f,0xc7},
	{0xa0,0xc8},
	{0xa1,0x00},
	{0xa2,0x00},
	{0x86,0x00},
	{0x87,0x00},
	{0x88,0x00},
	{0x89,0x00},
	{0xa4,0xb9},
	{0xa5,0xa0},
	{0xa6,0xba},
	{0xa7,0x92},
	{0xa9,0xba},
	{0xaa,0x80},
	{0xab,0x9d},
	{0xac,0x7f},
	{0xae,0xbb},
	{0xaf,0x9d},
	{0xb0,0xc8},
	{0xb1,0x97},
	{0xb3,0xb7},
	{0xb4,0x7f},
	{0xb5,0x00},
	{0xb6,0x00},
	{0x8b,0x00},
	{0x8c,0x00},
	{0x8d,0x00},
	{0x8e,0x00},
	{0x94,0x55},
	{0x99,0xa6},
	{0x9e,0xaa},
	{0xa3,0x0a},
	{0x8a,0x00},
	{0xa8,0x55},
	{0xad,0x55},
	{0xb2,0x55},
	{0xb7,0x05},
	{0x8f,0x00},
	{0xb8,0xcb},
	{0xb9,0x9b},  
	
/*	 
	{0xa4,0xb9}, //default AWB
	{0xa5,0xa0},
	{0x90,0xc9}, 
	{0x91,0xbe},                                 
	{0xa6,0xb8}, 
	{0xa7,0x95}, 
	{0x92,0xe6}, 
	{0x93,0xca},                                
	{0xa9,0xbc}, 
	{0xaa,0x95}, 
	{0x95,0x23}, 
	{0x96,0xe7},                                  
	{0xab,0x9d}, 
	{0xac,0x80},
	{0x97,0x43}, 
	{0x98,0x24},                                
	{0xae,0xb7}, 
	{0xaf,0x9e}, 
	{0x9a,0x43},
	{0x9b,0x24},                                 
	{0xb0,0xc8}, 
	{0xb1,0x97},
	{0x9c,0xc4}, 
	{0x9d,0x44},                               
	{0xb3,0xb7}, 
	{0xb4,0x7f},
	{0x9f,0xc7},
	{0xa0,0xc8},                               
	{0xb5,0x00}, 
	{0xb6,0x00},
	{0xa1,0x00},
	{0xa2,0x00},                         
	{0x86,0x60},
	{0x87,0x08},
	{0x88,0x00},
	{0x89,0x00},
	{0x8b,0xde},
	{0x8c,0x80},
	{0x8d,0x00},
	{0x8e,0x00},                               
	{0x94,0x55},
	{0x99,0xa6},
	{0x9e,0xaa},
	{0xa3,0x0a},
	{0x8a,0x0a},
	{0xa8,0x55},
	{0xad,0x55},
	{0xb2,0x55},
	{0xb7,0x05},
	{0x8f,0x05},                              
	{0xb8,0xcc},
	{0xb9,0x9a},
*/  

	/////////////////////////////////////////////////
	////////////////////  CC ////////////////////////
	/////////////////////////////////////////////////
	{0xfe,0x01},
	
	{0xd0,0x38},//skin red
	{0xd1,0x00},
	{0xd2,0x02},
	{0xd3,0x04},
	{0xd4,0x38},
	{0xd5,0x12},	
/*                     
	{0xd0,0x38},//skin white
	{0xd1,0xfd},
	{0xd2,0x06},
	{0xd3,0xf0},
	{0xd4,0x40},
	{0xd5,0x08},
*/
	
/*                       
	{0xd0,0x38},//guodengxiang
	{0xd1,0xf8},
	{0xd2,0x06},
	{0xd3,0xfd},
	{0xd4,0x40},
	{0xd5,0x00},	
*/
	{0xd6,0x30},
	{0xd7,0x00},
	{0xd8,0x0a},
	{0xd9,0x16},
	{0xda,0x39},
	{0xdb,0xf8},
	/////////////////////////////////////////////////
	////////////////////   LSC   ////////////////////
	/////////////////////////////////////////////////
	{0xfe,0x01}, 
	{0xc1,0x3c},
	{0xc2,0x50},
	{0xc3,0x00},
	{0xc4,0x40},
	{0xc5,0x30},
	{0xc6,0x30},
	{0xc7,0x10},
	{0xc8,0x00},
	{0xc9,0x00},
	{0xdc,0x20},
	{0xdd,0x10},
	{0xdf,0x00}, 
	{0xde,0x00}, 


	/////////////////////////////////////////////////
	///////////////////  Histogram  /////////////////
	/////////////////////////////////////////////////
	{0x01,0x10}, 
	{0x0b,0x31}, 
	{0x0e,0x50}, 
	{0x0f,0x0f}, 
	{0x10,0x6e}, 
	{0x12,0xa0}, 
	{0x15,0x60}, 
	{0x16,0x60}, 
	{0x17,0xe0}, 


	/////////////////////////////////////////////////
	//////////////   Measure Window   ///////////////
	/////////////////////////////////////////////////
	{0xcc,0x0c}, 
	{0xcd,0x10}, 
	{0xce,0xa0}, 
	{0xcf,0xe6}, 


	/////////////////////////////////////////////////
	/////////////////   dark sun   //////////////////
	/////////////////////////////////////////////////
	{0x45,0xf7},
	{0x46,0xff}, 
	{0x47,0x15},
	{0x48,0x03}, 
	{0x4f,0x60}, 
                                
	/////////////////////////////////////////////////
	///////////////////  banding  ///////////////////
	/////////////////////////////////////////////////
	{0xfe,0x00},
	{0x05,0x01},
	{0x06,0x12}, //HB
	{0x07,0x00},
	{0x08,0x1c}, //VB
	{0xfe,0x01},
	{0x25,0x00}, //step 
	{0x26,0x1f}, 
	{0x27,0x01}, //6fps
	{0x28,0xf0},  
	{0x29,0x01}, //6fps
	{0x2a,0xf0}, 
	{0x2b,0x01}, //6fps
	{0x2c,0xf0}, 
	{0x2d,0x03}, //3.3fps
	{0x2e,0xe0},
	{0x3c,0x20},
	/////////////////////  SPI   ////////////////////
	/////////////////////////////////////////////////
	{0xfe,0x03},
	{0x01,0x00},
	{0x02,0x00},
	{0x10,0x00},
	{0x15,0x00},
	{0x17,0x00}, //01//03
	{0x04,0x10},//fifo full level
	{0x40,0x00},
    {0x52,0x82}, //zwb 02改成da
	{0x53,0x24}, //24
	{0x54,0x20},
	{0x55,0x20}, //QQ//01
	{0x5a,0x00}, //00 //yuv 
#if 1//def DSS_AIR202_LUA_CAMERA
	{0x5b,0x40},
	{0x5c,0x01},
	{0x5d,0xf0},
	{0x5e,0x00},
#else
	{0x5b,0x80},
    {0x5c,0x02},
    {0x5d,0xe0},
    {0x5e,0x01},
#endif
  {0x51,0x03},
	

	{0xfe,0x00},
};

const AMOPENAT_CAMERA_REG cameraInitReg [] =
{
//bf3a01 initial
//initial black
{0xfe, 0x00},
{0x3d, 0x00},
{0x30, 0x3b},
{0x31, 0x3b},
{0x34, 0x01},
{0x35, 0x0a},
             
{0xfe, 0x01},
{0xe0, 0xba},//VCLK=48M//9a
{0xe1, 0x03},
{0xe2, 0x05},
{0xe3, 0x42},//bit[1]=1,bypass PLL
{0xe4, 0x22},
{0xe5, 0x03},//add lzq
             
{0xe7, 0x05},
             
//{0x51, 0x00, 0x01},//VSYNC:active low  HSYNC:active high
{0x50, 0x00},//SP mode
{0x66, 0x01},
{0x52, 0xf1},
{0x53, 0x20},//10
{0x5d, 0x00},
             
{0xfe, 0x00},
{0x00, 0x4b},//bit[5]:1,mirror;bit[4]:1,flip;
{0x02, 0x10},//dummy pixel
{0x15, 0x0a},
{0x3c, 0x9d},//0x9a bit[3:0]:black lock 20140918
{0x41, 0x02},// anti blc vibration
{0x3e, 0x68},
{0x0f, 0x13},
	           
//initial AWB
{0xfe, 0x00},
{0xa0, 0x54},
{0xb0, 0x19},//blue gain
{0xb1, 0x2d},//red gain
             
//initial AE 
{0xfe, 0x01},
{0x00, 0x08},
{0x0e, 0x03},
{0x0f, 0x30},//{0x0e,0x0f}:exposure time
{0x10, 0x18},//global gain
             
{0xfe, 0x00},
{0x84, 0xa2},//denoise; bit[7:4],the larger the smaller noise
{0x82, 0x08},//denoise; The larger the smaller noise;
{0x86, 0x23},//bit[6:4]:bright edge enhancement;bit[2:0]:dark edge enhancement;
             
//gamma tongtouxing hao
{0xfe, 0x00},
{0x60, 0x25},
{0x61, 0x2a},
{0x62, 0x28},
{0x63, 0x28},
{0x64, 0x20},
{0x65, 0x1d},
{0x66, 0x17},
{0x67, 0x15},
{0x68, 0x0f},
{0x69, 0x0e},
{0x6a, 0x0a},
{0x6b, 0x06},
{0x6c, 0x05},
{0x6d, 0x04},
{0x6e, 0x02},
             
{0x72, 0x0c},//gamma offset
{0x73, 0x0c},//gamma offset
{0x74, 0x44},//bit[7:4] and bit[3:0]?μ??′ó￡???ì????ì
	           
/*//gamma default
{0xfe, 0x00},
{0x60, 0x38},
{0x61, 0x30},
{0x62, 0x24},
{0x63, 0x1f},
{0x64, 0x1c},
{0x65, 0x16},
{0x66, 0x12},
{0x67, 0x0f},
{0x68, 0x0d},
{0x69, 0x0c},
{0x6a, 0x0b},
{0x6b, 0x09},
{0x6c, 0x09},
{0x6d, 0x08},
{0x6e, 0x07},
             
//gamma low noise
{0xfe, 0x00},
{0x60, 0x24},
{0x61, 0x30},
{0x62, 0x24},
{0x63, 0x1d},
{0x64, 0x1a},
{0x65, 0x14},
{0x66, 0x11},
{0x67, 0x0f},
{0x68, 0x0e},
{0x69, 0x0d},
{0x6a, 0x0c},
{0x6b, 0x0b},
{0x6c, 0x0a},
{0x6d, 0x09},
{0x6e, 0x09},*/
             
//outdoor color
{0xfe, 0x00},
{0xc7, 0x21},
{0xc8, 0x19},
{0xc9, 0x84},
{0xca, 0x64},
{0xcb, 0x89},
{0xcc, 0x3f},
{0xcd, 0x16},
             
//indoor color default
{0xfe, 0x00},
{0xc0, 0x05},
{0xc1, 0x07},
{0xc2, 0x30},
{0xc3, 0x28},
{0xc4, 0x3c},
{0xc5, 0x10},
{0xc6, 0x96},
             
/*//indoor color vivid green OK
{0xfe, 0x00},
{0xc0, 0x26},
{0xc1, 0x1b},
{0xc2, 0x5e},
{0xc3, 0x58},
{0xc4, 0x7d},
{0xc5, 0x2b},
{0xc6, 0x96},  */
             
/*//indoor color vivid blue OK
{0xfe, 0x00},
{0xc0, 0x39},
{0xc1, 0x31},
{0xc2, 0x76},
{0xc3, 0x64},
{0xc4, 0x57},
{0xc5, 0x07},
{0xc6, 0x96},
*/           
             
//AWB        
{0xfe, 0x00},
{0xb2, 0x01},
{0xb3, 0x11},//green gain
{0xa2, 0x11},//low limit of blue gain in indoor scene //0x18 20140918
{0xa3, 0x36},//high limit of blue gain in indoor scene
{0xa4, 0x11},//low limit of red gain in indoor scene
{0xa5, 0x36},//high limit of red gain in indoor scene
{0xa7, 0x80},//blue target
{0xa8, 0x7f},//red target
{0xa9, 0x15},
{0xaa, 0x10},
{0xab, 0x10},
{0xac, 0x2c},
{0xad, 0xf0},
{0xae, 0x20},//0x80->0x20 Ycbcr LIMIT
{0xb4, 0x18},//low limit of blue gain in outdoor scene
{0xb5, 0x1a},//high limit of blue gain in outdoor scene
{0xb6, 0x1c},//low limit of red gain in outdoor scene
{0xb7, 0x30},//high limit of red gain in outdoor scene
{0xd0, 0x4c},
             
//AE         
{0xfe, 0x01},
{0x04, 0x4f},//AE target
{0x09, 0x0d},//bit[5:0]:max integration time step
{0x0a, 0x45},
{0x0b, 0x82},//minimum integration time
{0x0c, 0x2e},//50hz banding
{0x0d, 0x26},//60hz banding
{0x15, 0x42},
{0x17, 0xb5},
{0x18, 0x28},//The value should be smaller than the 50hz banding. add 20140702
{0x1b, 0x28},//minimum global gain
{0x1c, 0x50},
{0x1d, 0x39},
{0x1e, 0x5d},
{0x1f, 0x77},//max global gain
             
{0xfe, 0x00},
{0xce, 0x48},//Contrast
             
//saturation
{0xfe, 0x01},
{0x64, 0xc0},//blue saturation
{0x65, 0xb0},//red saturation
             
{0xfe, 0x01}, 
{0x59, 0x00},//bit[3:0]: skip frame counter 0, don't skip frame,else skip as many as FRAME_CNT_REG frames
//delay 400ms 
{0xfe, 0x00},
{0xfe, 0x00},
{0x3d, 0xff},
{0xa0, 0x55},
{0xfe, 0x01},
{0x00, 0x05},


	
};






	/////////////////////////////////////////////////
	/////////////////   CISCTL reg  /////////////////
	/////////////////////////////////////////////////




	/////////////////////////////////////////////////
	////////////////////   BLK   ////////////////////
	/////////////////////////////////////////////////


	/////////////////////////////////////////////////
	//////////////////   ISP reg  ///////////////////
	/////////////////////////////////////////////////
	
	/////////////////////////////////////////////////
	///////////////////   GAIN   ////////////////////
	/////////////////////////////////////////////////


	///////////////////////////////////////////////// 
	///////////////////   DNDD  /////////////////////
	///////////////////////////////////////////////// 
	///////////////////////////////////////////////// 
	//////////////////   EEINTP  ////////////////////
	///////////////////////////////////////////////// 
	///////////////////////////////////////////////// 
	/////////////////////  ASDE  ////////////////////
	///////////////////////////////////////////////// 

	 
	 

	/////////////////////////////////////////////////
	///////////////////   GAMMA   ///////////////////
	/////////////////////////////////////////////////
	
/* 
	{0xfe,0x00},//big gamma
	{0xbf,0x08},
	{0xc0,0x1d},
	{0xc1,0x34},
	{0xc2,0x4b},
	{0xc3,0x60},
	{0xc4,0x73},
	{0xc5,0x85},
	{0xc6,0x9f},
	{0xc7,0xb5},
	{0xc8,0xc7},
	{0xc9,0xd5},
	{0xca,0xe0},
	{0xcb,0xe7},
	{0xcc,0xec},
	{0xcd,0xf4},
	{0xce,0xfa},
	{0xcf,0xff},
*/	

/*
	{0xfe,0x00},//small gamma
	{0xbf,0x08},
	{0xc0,0x18},
	{0xc1,0x2c},
	{0xc2,0x41},
	{0xc3,0x59},
	{0xc4,0x6e},
	{0xc5,0x81},
	{0xc6,0x9f},
	{0xc7,0xb5},
	{0xc8,0xc7},
	{0xc9,0xd5},
	{0xca,0xe0},
	{0xcb,0xe7},
	{0xcc,0xec},
	{0xcd,0xf4},
	{0xce,0xfa},
	{0xcf,0xff},
*/
	/////////////////////////////////////////////////
	///////////////////   YCP  //////////////////////
	/////////////////////////////////////////////////

	/////////////////////////////////////////////////
	////////////////////   AEC   ////////////////////
	/////////////////////////////////////////////////


	/////////////////////////////////////////////////
	////////////////////   AWB   ////////////////////
	/////////////////////////////////////////////////

	
/*	 
	{0xa4,0xb9}, //default AWB
	{0xa5,0xa0},
	{0x90,0xc9}, 
	{0x91,0xbe},                                 
	{0xa6,0xb8}, 
	{0xa7,0x95}, 
	{0x92,0xe6}, 
	{0x93,0xca},                                
	{0xa9,0xbc}, 
	{0xaa,0x95}, 
	{0x95,0x23}, 
	{0x96,0xe7},                                  
	{0xab,0x9d}, 
	{0xac,0x80},
	{0x97,0x43}, 
	{0x98,0x24},                                
	{0xae,0xb7}, 
	{0xaf,0x9e}, 
	{0x9a,0x43},
	{0x9b,0x24},                                 
	{0xb0,0xc8}, 
	{0xb1,0x97},
	{0x9c,0xc4}, 
	{0x9d,0x44},                               
	{0xb3,0xb7}, 
	{0xb4,0x7f},
	{0x9f,0xc7},
	{0xa0,0xc8},                               
	{0xb5,0x00}, 
	{0xb6,0x00},
	{0xa1,0x00},
	{0xa2,0x00},                         
	{0x86,0x60},
	{0x87,0x08},
	{0x88,0x00},
	{0x89,0x00},
	{0x8b,0xde},
	{0x8c,0x80},
	{0x8d,0x00},
	{0x8e,0x00},                               
	{0x94,0x55},
	{0x99,0xa6},
	{0x9e,0xaa},
	{0xa3,0x0a},
	{0x8a,0x0a},
	{0xa8,0x55},
	{0xad,0x55},
	{0xb2,0x55},
	{0xb7,0x05},
	{0x8f,0x05},                              
	{0xb8,0xcc},
	{0xb9,0x9a},
*/  

	/////////////////////////////////////////////////
	////////////////////  CC ////////////////////////
	/////////////////////////////////////////////////
	
/*                     
	{0xd0,0x38},//skin white
	{0xd1,0xfd},
	{0xd2,0x06},
	{0xd3,0xf0},
	{0xd4,0x40},
	{0xd5,0x08},
*/
	
/*                       
	{0xd0,0x38},//guodengxiang
	{0xd1,0xf8},
	{0xd2,0x06},
	{0xd3,0xfd},
	{0xd4,0x40},
	{0xd5,0x00},	
*/
	/////////////////////////////////////////////////
	////////////////////   LSC   ////////////////////
	/////////////////////////////////////////////////


	/////////////////////////////////////////////////
	///////////////////  Histogram  /////////////////
	/////////////////////////////////////////////////


	/////////////////////////////////////////////////
	//////////////   Measure Window   ///////////////
	/////////////////////////////////////////////////


	/////////////////////////////////////////////////
	/////////////////   dark sun   //////////////////
	/////////////////////////////////////////////////
                                
	/////////////////////////////////////////////////
	///////////////////  banding  ///////////////////
	/////////////////////////////////////////////////
	/////////////////////  SPI   ////////////////////
	/////////////////////////////////////////////////


static BOOL isScanning = FALSE;
static BOOL gScanOpen = FALSE;

//此处的内存分配还存在下面两个问题需要优化:
//1、如果lua脚本中没有加载disp.init，framebuffer就为NULL，此处动态分配，会导致扫码scannopen里面死机
//2、即使lua脚本中加载了disp.init，但是framebuffer的大小不满足此处的需求，也需要此处动态分配，会导致扫码scannopen里面死机
static void allocScannerBuffer()
{
    extern  u8 *framebuffer;
    extern u16 frameBufferSize;
    
    if(g_scanner_buff==NULL)
    {
        g_scanner_buff = framebuffer;
        if((g_scanner_buff==NULL) || frameBufferSize<(CAMERA_PREVIEW_WITHE*CAMERA_PREVIEW_HEIGHT/4))
        {
            //g_scanner_buff = malloc(CAMERA_PREVIEW_WITHE*CAMERA_PREVIEW_HEIGHT/4);
            g_scanner_buff = (u8*)((u32)malloc(CAMERA_PREVIEW_WITHE*CAMERA_PREVIEW_HEIGHT/4) | 0xa0000000);
            if (g_scanner_buff==NULL)
            {
                IVTBL(print)("camera_yuv_conver_y malloc error");
            }
            //IVTBL(print)("g_scanner_buff=%x",g_scanner_buff);
        }
    }
}

static char * camera_yuv_conver_y(unsigned char *data)
{
    // 解析黑白图片， 减少解析的一半图片
    unsigned char *src = data,*end = data + (CAMERA_PREVIEW_WITHE/2*CAMERA_PREVIEW_HEIGHT/2*2);
    allocScannerBuffer();
    
    unsigned char *dst = g_scanner_buff;
    while (src < end)
    {
        *dst = *src;
        src += 2;
        dst++;
    }//End of while;

//#ifdef CAMERA_MIRROR
    if (sMirror)
    {
	    int i,j;
	    dst = g_scanner_buff;
	    for(i=0; i<ZBAR_IMAGE_END_Y; i++)
	    {
	        for(j=0; j<ZBAR_IMAGE_END_X/2; j++)
	        {
	            unsigned char temp = *(dst+i*ZBAR_IMAGE_END_X+ZBAR_IMAGE_END_X-1-j);
	            *(dst+i*ZBAR_IMAGE_END_X+ZBAR_IMAGE_END_X-1-j) = *(dst+i*ZBAR_IMAGE_END_X+j);
	            *(dst+i*ZBAR_IMAGE_END_X+j) = temp;
	        }
	    }
    }
//#endif
    
    return g_scanner_buff;
}

void zbar_scanner_run(int width, int height, int size, char *dataInput)
{
    int len;
    char *data;
    char *type;    
    
    //创建句柄， handle != 0 表示解码成功
    //IVTBL(print)("zbar_scanner_run %d,%d,%d,%x", width, height, size, dataInput);
    int handle = IVTBL(zbar_scannerOpen)(width, height, size, dataInput);    
    
    //IVTBL(print)("zbar_scanner_run result %d", handle);
    //ASSERT(0);
    
    if (handle)	
    {    
        // 解码成功获取二维码信息
        data = IVTBL(zbar_getData)(handle, &len);
        type = IVTBL(zbar_getType)(handle);

        //关闭预览，否则会一直有camera中断上来，下面的消息不会得到执行
        platform_camera_preview_close();
        
        //发送消息通知lua
        PlatformMessage *rtosmsg = platform_calloc(1, sizeof(PlatformMessage));
        rtosmsg->id = RTOS_MSG_ZBAR;
        rtosmsg->data.zbarData.result = TRUE;
        rtosmsg->data.zbarData.pType = platform_malloc(strlen(type)+1);
        memset(rtosmsg->data.zbarData.pType, 0, strlen(type)+1);
        memcpy(rtosmsg->data.zbarData.pType, type, strlen(type));
        rtosmsg->data.zbarData.pData = platform_malloc(len+1);
        memset(rtosmsg->data.zbarData.pData, 0, len+1);
        memcpy(rtosmsg->data.zbarData.pData, data, len);
        //IVTBL(print)("zbar_scanner_run %s, %s, %d", type, rtosmsg->data.zbarData.pData, len);
        
        //关闭扫描
        IVTBL(zbar_scannerClose)(handle);		
        platform_rtos_send(rtosmsg);	
    }
    else
    {
        //rtosmsg->data.zbarData.result = FALSE;
        isScanning = FALSE;
    }		 
}



// camera 回调处理
static void camera_event_callback(T_AMOPENAT_CAMERA_MESSAGE *pMsg)
{

    switch(pMsg->evtId)
    {
        case OPENAT_DRV_EVT_VIDEORECORD_FINISH_IND:
        {
            //iot_debug_print("[zbar] camera_evevt_callback videorecordFinishResult %d", pMsg->param.videorecordFinishResult);  
            break;
        }
        case OPENAT_DRV_EVT_CAMERA_DATA_IND:
        {
            unsigned char *pData = (unsigned char *)pMsg->dataParam.data;
            
            //IVTBL(print)("camera_event_callback %x,%d,%d", pData,isScanning,IVTBL(get_system_tick)());
            
            if (pData &&  !isScanning)
            {
                 cust_camera_zbar_message(ZBAR_IMAGE_END_X - ZBAR_IMAGE_START_X,
                     ZBAR_IMAGE_END_Y - ZBAR_IMAGE_START_Y,
                     (ZBAR_IMAGE_END_Y - ZBAR_IMAGE_START_Y) * (ZBAR_IMAGE_END_X - ZBAR_IMAGE_START_X),
                     camera_yuv_conver_y(pData));
                 isScanning = TRUE;
            }
            
            break;
        }
        default:
        break;
    }
}

BOOL camera_poweron(BOOL video_mode)
{
  BOOL res=FALSE;
  T_AMOPENAT_CAMERA_PARAM initParam =
  {
      NULL,
      OPENAT_I2C_2, 
      0x6e,
      AMOPENAT_CAMERA_REG_ADDR_8BITS|AMOPENAT_CAMERA_REG_DATA_8BITS,
      
      TRUE,
      FALSE,
      FALSE, 
      
      240,
      320,
      
      CAMERA_IMAGE_FORMAT_YUV422,
      cameraInitReg,
      sizeof(cameraInitReg)/sizeof(AMOPENAT_CAMERA_REG),
      {0xfd, CAM_BY3A01_ID},
/*+\new\zhuwangbin\2018.5.9\à??1é???í·3?ê??ˉ2?êy, ?a??????é???í·2???èY?êìa*/
      {OPENAT_GPIO_2,OPENAT_GPIO_3,FALSE},
      5,
      OPENAT_SPI_MODE_MASTER2_1,
      OPENAT_SPI_OUT_V0_Y1_U0_Y0
/*-\new\zhuwangbin\2018.5.9\à??1é???í·3?ê??ˉ2?êy, ?a??????é???í·2???èY?êìa*/

/*-\new\zhuwangbin\2018.5.9\扩展摄像头初始化参数, 解决其他摄像头不兼容问题*/
  };

  res = IVTBL(camera_init)(&initParam);


  IVTBL(print)("ljdcam res=%d",res);

  if (!res )
  {
      return res;
  }

  return IVTBL(camera_poweron)(video_mode);
} 

BOOL camera_poweron_gc0310(BOOL video_mode, BOOL bZbarScan, BOOL bMirror)
{
  BOOL res=FALSE;
  sMirror = bMirror;
  cameraInitReg_gc0310[28].value = bMirror ? 0x15 : 0x14;
  T_AMOPENAT_CAMERA_PARAM initParam =
  {
      bZbarScan ? camera_event_callback : NULL,
      OPENAT_I2C_3, 
      CAMERA_I2C_ADDR,
      AMOPENAT_CAMERA_REG_ADDR_8BITS|AMOPENAT_CAMERA_REG_DATA_8BITS,
      
      TRUE,
      FALSE,
      FALSE, 
#if 1//def DSS_AIR202_LUA_CAMERA
      640,
      480,
#else
	  240,
	  320,
#endif
      CAMERA_IMAGE_FORMAT_YUV422,
      cameraInitReg_gc0310,
      sizeof(cameraInitReg_gc0310)/sizeof(AMOPENAT_CAMERA_REG),
      {CAM_ID_REG, CAM_GC0310},
      {OPENAT_GPIO_19,OPENAT_GPIO_20,TRUE},
      8,
      OPENAT_SPI_MODE_MASTER2_2,
      OPENAT_SPI_OUT_V0_Y1_U0_Y0,
      TRUE
/*-\new\zhuwangbin\2018.5.9\扩展摄像头初始化参数, 解决其他摄像头不兼容问题*/
  };

  res = IVTBL(camera_init)(&initParam);

  IVTBL(print)("ljdcam res=%d",res);

  #if 0
  if (!res )
  {
      return res;
  }
  #endif

  return IVTBL(camera_poweron)(video_mode);
}

// 获取camera得到的数据， 发送到zbartask 去解析
BOOL platform_camera_poweron(BOOL video_mode, int nCamType, BOOL bZbarScan, BOOL bMirror)
{
    gScanOpen = bZbarScan;
    if(bZbarScan)
    {
        allocScannerBuffer();        
    }
    if (nCamType==1)
    {
        return camera_poweron_gc0310(video_mode, bZbarScan, bMirror);
    }
    
    return camera_poweron(video_mode);
}


BOOL platform_camera_poweroff(void)
{
    isScanning = FALSE;    
    return IVTBL(camera_poweroff)();
}


BOOL platform_camera_preview_open(u16 offsetx, u16 offsety, u16 startx, u16 starty, u16 endx, u16 endy)
{
    T_AMOPENAT_CAM_PREVIEW_PARAM previewParam;
    previewParam.startX = startx;
    previewParam.startY = starty;
    previewParam.endX = endx;
    previewParam.endY = endy;
    previewParam.offsetX = offsetx;
    previewParam.offsetY = offsety;
    previewParam.recordAudio = FALSE;

    if(gScanOpen)
    {
        IVTBL(exit_deepsleep)();
        IVTBL(sys_request_freq)(OPENAT_SYS_FREQ_312M);
    }
    
    return IVTBL(camera_preview_open)(&previewParam);
}



BOOL platform_camera_preview_close(void)
{
    isScanning = FALSE;
    if(gScanOpen)
    {
        IVTBL(exit_deepsleep)();
        IVTBL(sys_request_freq)(OPENAT_SYS_FREQ_208M);
    }
    return IVTBL(camera_preview_close)();
}


BOOL platform_camera_capture(u16 width, u16 height)
{
    T_AMOPENAT_CAM_CAPTURE_PARAM captureParam;
    captureParam.imageWidth = width;
    captureParam.imageHeight = height;
    
    return IVTBL(camera_capture)(&captureParam);
}


BOOL platform_camera_save_photo(const char* filename)
{
    int length;
    UINT16 *unicode_path;
    char* actname;
    int fb;
    BOOL result;
    if((find_dm_entry( filename, &actname ) ) == -1 )
    {
        return -1;
    }
    


    IVTBL(delete_file)(actname);
    fb = IVTBL(create_file)(actname, NULL);


    result = IVTBL(camera_save_photo)(fb );
    IVTBL(close_file)(fb);
    return result;
}

int platform_encodeJpegBuffer(UINT8 *inFilename,
                                int inFormat,
                                int inWidth,
                                int inHeight,
                                int outWidth,
                                int outHeight,
                                int inQuality,
                                char *outFilename)
{
    T_AMOPENAT_DECODE_INPUT_PARAM inputParam;
    UINT32 inFIleSize;
    UINT32 readLen = 0;
    UINT8 *output;
    UINT32 outputLen;
    int err, inFb, outFb;
    char* inActname, *outActname;

   IVTBL(print)("img Imgs_encodeJpegBuffer %s,%d,%d,%d,%d,%d,%d,%s", 
        inFilename, inFormat, 
        inWidth, inHeight, outWidth, outHeight, 
        inQuality, outFilename);

    if(find_dm_entry(inFilename, &inActname ) == -1 
          || find_dm_entry(outFilename, &outActname) == -1)
    {
        return -1;
    }

    inFb =  IVTBL(open_file)(inActname, O_RDONLY, 0);
    if (inFb < 0)
    {
      return -2;
    }

    inFIleSize = IVTBL(seek_file)(inFb, 0, SEEK_END);
    
    if (inFIleSize <= 0)
    {
      IVTBL(close_file)(inActname);
      return -3;
    }
    IVTBL(seek_file)(inFb, 0, SEEK_SET);

    inputParam.inBuffer = IVTBL(malloc)(inFIleSize);

    if (!inputParam.inBuffer)
    {
      IVTBL(close_file)(inActname);
      IVTBL(delete_file)(inActname);
      return -4;
    }
    
    readLen = IVTBL(read_file)(inFb, inputParam.inBuffer, inFIleSize);
   
    inputParam.inFormat = inFormat;
    inputParam.inHeight = inHeight;
    inputParam.inWidth = inWidth;
    inputParam.outHeight = outHeight;
    inputParam.outWidth = outWidth;
    inputParam.inQuality = inQuality;

    IVTBL(delete_file)(outActname);
    outFb = IVTBL(create_file)(outActname, NULL);
  
    if (outFb < 0)
    {
      return -5;
    } 

    output = inputParam.inBuffer;
    err = IVTBL(Imgs_encodeJpegBuffer)(
        &inputParam,
        output,
        &outputLen);

    IVTBL(write_file)(outFb, output+1, outputLen);
    
    if (err != 0)
    {
      return err;
    }
    
    IVTBL(close_file)(outFb);
    IVTBL(print)("img Imgs_encodeJpegBuffer %d, err %d", outputLen,  err);
    IVTBL(free)(inputParam.inBuffer);
    
    return err;
 
}
#endif

