#ifndef __DEBUG_H__
#define __DEBUG_H__
#define DBG(x, y...) printf("%s %d:"x"\r\n", __FUNCTION__, __LINE__, ##y)
#endif
