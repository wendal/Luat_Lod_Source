/**************************************************************************
 *              Copyright (C), AirM2M Tech. Co., Ltd.
 *
 * Name:    fcntl.h
 * Author:  liweiqiang
 * Version: V0.1
 * Date:    2012/11/27
 *
 * Description:
 * 
 **************************************************************************/

#ifndef __FCNTL_H__
#define __FCNTL_H__

#define O_RDONLY (0)
#define O_WRONLY (1)
#define O_RDWR (2)

#ifndef O_CREAT
#define O_CREAT (0x200)  
#endif

#ifndef O_EXCL
#define O_EXCL (0x800)  
#endif

#ifndef O_TRUNC
#define O_TRUNC (0x400)  
#endif

#ifndef O_APPEND
#define O_APPEND (8)
#endif

#endif //__FCNTL_H__