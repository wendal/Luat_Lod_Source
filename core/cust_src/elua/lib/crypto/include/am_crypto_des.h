/**
 * \file des.h
 *
 * \brief DES block cipher
 *
 *  Copyright (C) 2006-2015, ARM Limited, All Rights Reserved
 *  SPDX-License-Identifier: Apache-2.0
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); you may
 *  not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 *  WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *  This file is part of mbed TLS (https://tls.mbed.org)
 */
#ifndef AM_CRYPTO_MBEDTLS_DES_H
#define AM_CRYPTO_MBEDTLS_DES_H

//#include "stdint.h"
#include "stddef.h"
#include <type.h>


#define AM_CRYPTO_MBEDTLS_CIPHER_MODE_CBC

#define AM_CRYPTO_MBEDTLS_DES_ENCRYPT     1
#define AM_CRYPTO_MBEDTLS_DES_DECRYPT     0

#define AM_CRYPTO_MBEDTLS_ERR_DES_INVALID_INPUT_LENGTH              -0x0032  /**< The data input has an invalid length. */

#define AM_CRYPTO_MBEDTLS_DES_KEY_SIZE    8

#if !defined(MBEDTLS_DES_ALT)
// Regular implementation
//

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \brief          DES context structure
 */
typedef struct
{
    unsigned int sk[32];            /*!<  DES subkeys       */
}
am_crypto_mbedtls_des_context;

/**
 * \brief          Triple-DES context structure
 */
typedef struct
{
    unsigned int sk[96];            /*!<  3DES subkeys      */
}
am_crypto_mbedtls_des3_context;

/**
 * \brief          Initialize DES context
 *
 * \param ctx      DES context to be initialized
 */
void am_crypto_mbedtls_des_init( am_crypto_mbedtls_des_context *ctx );

/**
 * \brief          Clear DES context
 *
 * \param ctx      DES context to be cleared
 */
void am_crypto_mbedtls_des_free( am_crypto_mbedtls_des_context *ctx );

/**
 * \brief          Initialize Triple-DES context
 *
 * \param ctx      DES3 context to be initialized
 */
void am_crypto_mbedtls_des3_init( am_crypto_mbedtls_des3_context *ctx );

/**
 * \brief          Clear Triple-DES context
 *
 * \param ctx      DES3 context to be cleared
 */
void am_crypto_mbedtls_des3_free( am_crypto_mbedtls_des3_context *ctx );

/**
 * \brief          Set key parity on the given key to odd.
 *
 *                 DES keys are 56 bits long, but each byte is padded with
 *                 a parity bit to allow verification.
 *
 * \param key      8-byte secret key
 */
void am_crypto_mbedtls_des_key_set_parity( unsigned char key[AM_CRYPTO_MBEDTLS_DES_KEY_SIZE] );

/**
 * \brief          Check that key parity on the given key is odd.
 *
 *                 DES keys are 56 bits long, but each byte is padded with
 *                 a parity bit to allow verification.
 *
 * \param key      8-byte secret key
 *
 * \return         0 is parity was ok, 1 if parity was not correct.
 */
int am_crypto_mbedtls_des_key_check_key_parity( const unsigned char key[AM_CRYPTO_MBEDTLS_DES_KEY_SIZE] );

/**
 * \brief          Check that key is not a weak or semi-weak DES key
 *
 * \param key      8-byte secret key
 *
 * \return         0 if no weak key was found, 1 if a weak key was identified.
 */
int am_crypto_mbedtls_des_key_check_weak( const unsigned char key[AM_CRYPTO_MBEDTLS_DES_KEY_SIZE] );

/**
 * \brief          DES key schedule (56-bit, encryption)
 *
 * \param ctx      DES context to be initialized
 * \param key      8-byte secret key
 *
 * \return         0
 */
int am_crypto_mbedtls_des_setkey_enc( am_crypto_mbedtls_des_context *ctx, const unsigned char key[AM_CRYPTO_MBEDTLS_DES_KEY_SIZE] );

/**
 * \brief          DES key schedule (56-bit, decryption)
 *
 * \param ctx      DES context to be initialized
 * \param key      8-byte secret key
 *
 * \return         0
 */
int am_crypto_mbedtls_des_setkey_dec( am_crypto_mbedtls_des_context *ctx, const unsigned char key[AM_CRYPTO_MBEDTLS_DES_KEY_SIZE] );

/**
 * \brief          Triple-DES key schedule (112-bit, encryption)
 *
 * \param ctx      3DES context to be initialized
 * \param key      16-byte secret key
 *
 * \return         0
 */
int am_crypto_mbedtls_des3_set2key_enc( am_crypto_mbedtls_des3_context *ctx,
                      const unsigned char key[AM_CRYPTO_MBEDTLS_DES_KEY_SIZE * 2] );

/**
 * \brief          Triple-DES key schedule (112-bit, decryption)
 *
 * \param ctx      3DES context to be initialized
 * \param key      16-byte secret key
 *
 * \return         0
 */
int am_crypto_mbedtls_des3_set2key_dec( am_crypto_mbedtls_des3_context *ctx,
                      const unsigned char key[AM_CRYPTO_MBEDTLS_DES_KEY_SIZE * 2] );

/**
 * \brief          Triple-DES key schedule (168-bit, encryption)
 *
 * \param ctx      3DES context to be initialized
 * \param key      24-byte secret key
 *
 * \return         0
 */
int am_crypto_mbedtls_des3_set3key_enc( am_crypto_mbedtls_des3_context *ctx,
                      const unsigned char key[AM_CRYPTO_MBEDTLS_DES_KEY_SIZE * 3] );

/**
 * \brief          Triple-DES key schedule (168-bit, decryption)
 *
 * \param ctx      3DES context to be initialized
 * \param key      24-byte secret key
 *
 * \return         0
 */
int am_crypto_mbedtls_des3_set3key_dec( am_crypto_mbedtls_des3_context *ctx,
                      const unsigned char key[AM_CRYPTO_MBEDTLS_DES_KEY_SIZE * 3] );

/**
 * \brief          DES-ECB block encryption/decryption
 *
 * \param ctx      DES context
 * \param input    64-bit input block
 * \param output   64-bit output block
 *
 * \return         0 if successful
 */
int am_crypto_mbedtls_des_crypt_ecb( am_crypto_mbedtls_des_context *ctx,
                    const unsigned char input[8],
                    unsigned char output[8] );

#if defined(AM_CRYPTO_MBEDTLS_CIPHER_MODE_CBC)
/**
 * \brief          DES-CBC buffer encryption/decryption
 *
 * \note           Upon exit, the content of the IV is updated so that you can
 *                 call the function same function again on the following
 *                 block(s) of data and get the same result as if it was
 *                 encrypted in one call. This allows a "streaming" usage.
 *                 If on the other hand you need to retain the contents of the
 *                 IV, you should either save it manually or use the cipher
 *                 module instead.
 *
 * \param ctx      DES context
 * \param mode     AM_CRYPTO_MBEDTLS_DES_ENCRYPT or AM_CRYPTO_MBEDTLS_DES_DECRYPT
 * \param length   length of the input data
 * \param iv       initialization vector (updated after use)
 * \param input    buffer holding the input data
 * \param output   buffer holding the output data
 */
int am_crypto_mbedtls_des_crypt_cbc( am_crypto_mbedtls_des_context *ctx,
                    int mode,
                    size_t length,
                    unsigned char iv[8],
                    const unsigned char *input,
                    unsigned char *output );
#endif /* AM_CRYPTO_MBEDTLS_CIPHER_MODE_CBC */

/**
 * \brief          3DES-ECB block encryption/decryption
 *
 * \param ctx      3DES context
 * \param input    64-bit input block
 * \param output   64-bit output block
 *
 * \return         0 if successful
 */
int am_crypto_mbedtls_des3_crypt_ecb( am_crypto_mbedtls_des3_context *ctx,
                     const unsigned char input[8],
                     unsigned char output[8] );

#if defined(AM_CRYPTO_MBEDTLS_CIPHER_MODE_CBC)
/**
 * \brief          3DES-CBC buffer encryption/decryption
 *
 * \note           Upon exit, the content of the IV is updated so that you can
 *                 call the function same function again on the following
 *                 block(s) of data and get the same result as if it was
 *                 encrypted in one call. This allows a "streaming" usage.
 *                 If on the other hand you need to retain the contents of the
 *                 IV, you should either save it manually or use the cipher
 *                 module instead.
 *
 * \param ctx      3DES context
 * \param mode     AM_CRYPTO_MBEDTLS_DES_ENCRYPT or AM_CRYPTO_MBEDTLS_DES_DECRYPT
 * \param length   length of the input data
 * \param iv       initialization vector (updated after use)
 * \param input    buffer holding the input data
 * \param output   buffer holding the output data
 *
 * \return         0 if successful, or AM_CRYPTO_MBEDTLS_ERR_DES_INVALID_INPUT_LENGTH
 */
int am_crypto_mbedtls_des3_crypt_cbc( am_crypto_mbedtls_des3_context *ctx,
                     int mode,
                     size_t length,
                     unsigned char iv[8],
                     const unsigned char *input,
                     unsigned char *output );
#endif /* AM_CRYPTO_MBEDTLS_CIPHER_MODE_CBC */

/**
 * \brief          Internal function for key expansion.
 *                 (Only exposed to allow overriding it,
 *                 see am_crypto_mbedtls_des_setkey_ALT)
 *
 * \param SK       Round keys
 * \param key      Base key
 */
void am_crypto_mbedtls_des_setkey( unsigned int SK[32],
                         const unsigned char key[AM_CRYPTO_MBEDTLS_DES_KEY_SIZE] );
#ifdef __cplusplus
}
#endif

#else  /* MBEDTLS_DES_ALT */
#include "des_alt.h"
#endif /* MBEDTLS_DES_ALT */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \brief          Checkup routine
 *
 * \return         0 if successful, or 1 if the test failed
 */
 #ifdef MBEDTLS_SELF_TEST
int am_crypto_mbedtls_des_self_test( int verbose );
#endif

#ifdef __cplusplus
}
#endif

#endif /* des.h */
