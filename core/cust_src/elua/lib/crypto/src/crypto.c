/**************************************************************************
 *              Copyright (C), AirM2M Tech. Co., Ltd.
 *
 * Name:   crypto.c
 * Author:  zhutianhua
 * Date:    2018/5/15
 *
 * Description:
 *          lua.crypto库
 **************************************************************************/
#include "crypto.h"
#include "assert.h"
#define MD5_DIGEST_SIZE 16
#define META_NAME                 "crypto"
#define crypto_check( L )      ( iot_md5_context* )luaL_checkudata( L, 1, META_NAME )


static unsigned char l_iot_common_hb2hex(unsigned char hb)
{
    hb = hb & 0xF;
    return (unsigned char) (hb < 10 ? '0' + hb : hb - 10 + 'A');
}

static int l_crypto_base64_encode(lua_State *L)
{
    const char *inputData = luaL_checkstring(L,1);
    int inputLen = luaL_checkinteger(L, 2);
    u8 *outputData = NULL;
    u32 outputLen = 0;
    u32 outputLenMax = (inputLen/3+1)*4;

    luaL_Buffer b;
    luaL_buffinit( L, &b );
    
    if(outputLenMax > LUAL_BUFFERSIZE)
    {
        outputData = malloc(outputLenMax+1);
        memset(outputData,0,outputLenMax+1);
        aliyun_iot_common_base64encode(inputData, inputLen, outputLenMax, outputData, &outputLen);
        luaL_addlstring(&b,outputData,outputLen);
        free(outputData);
        outputData = NULL;
    }
    else
    {
        aliyun_iot_common_base64encode(inputData, inputLen, LUAL_BUFFERSIZE, b.p, &outputLen);
        b.p += outputLen;
    }
    
    luaL_pushresult( &b );
    return 1;
}


static int l_crypto_base64_decode(lua_State *L)
{
    const char *inputData = luaL_checkstring(L,1);
    int inputLen = luaL_checkinteger(L, 2);
    u8 *outputData = NULL;
    u32 outputLen = 0;
    u32 outputLenMax = inputLen*3/4+1;

    luaL_Buffer b;
    luaL_buffinit( L, &b );
    
    if(outputLenMax > LUAL_BUFFERSIZE)
    {
        outputData = malloc(outputLenMax+1);
        memset(outputData,0,outputLenMax+1);
        aliyun_iot_common_base64decode(inputData, inputLen, outputLenMax, outputData, &outputLen);
        luaL_addlstring(&b,outputData,outputLen);
        free(outputData);
        outputData = NULL;
    }
    else
    {
        aliyun_iot_common_base64decode(inputData, inputLen, LUAL_BUFFERSIZE, b.p, &outputLen);
        b.p += outputLen;
    }
    
    luaL_pushresult( &b );
    return 1;
}


static int l_crypto_hmac_md5(lua_State *L)
{
    const char *inputData = luaL_checkstring(L,1);
    int inputLen = luaL_checkinteger(L, 2);
    const char *signKey = luaL_checkstring(L,3);
    int signKeyLen = luaL_checkinteger(L, 4);    

    luaL_Buffer b;
    luaL_buffinit( L, &b );    

    memset(b.buffer,0,LUAL_BUFFERSIZE);
    aliyun_iot_common_hmac_md5(inputData, inputLen, b.p, signKey, signKeyLen);
    b.p += strlen(b.buffer);
    
    luaL_pushresult( &b );
    return 1;
}


static int l_crypto_flow_md5(lua_State *L)
{
    iot_md5_context* ctx= (iot_md5_context*)lua_newuserdata(L, sizeof(iot_md5_context));
    aliyun_iot_md5_init(ctx);
    aliyun_iot_md5_starts(ctx);
    luaL_getmetatable( L, META_NAME );
    lua_setmetatable( L, -2 );
    return 1;
}
static int l_crypto_md5_update(lua_State *L)
{
    iot_md5_context* ctx = (iot_md5_context*)crypto_check(L);
    size_t inputLen;
    const char *inputData = lua_tolstring(L,2,&inputLen);
    aliyun_iot_md5_update( ctx,(UINT8 *)inputData, inputLen);

    return 0;
}

static int l_crypto_md5_hexdigest(lua_State *L)
{
    luaL_Buffer b;
    iot_md5_context* ctx = (iot_md5_context*)crypto_check(L);
    luaL_buffinit( L, &b );    
       
    memset(b.buffer,0,LUAL_BUFFERSIZE);

    UINT8 out[MD5_DIGEST_SIZE];
	UINT32 i = 0;
    memset(out, 0, MD5_DIGEST_SIZE);   
    aliyun_iot_md5_finish( ctx, out);
    aliyun_iot_md5_free( ctx );
    for (i = 0; i < MD5_DIGEST_SIZE; ++i)
    {
        b.p[i * 2] = aliyun_iot_common_hb2hex(out[i] >> 4);
        b.p[i * 2 + 1] = aliyun_iot_common_hb2hex(out[i]);
    }
    b.p += strlen(b.buffer);
    luaL_pushresult( &b );
    return 1;

}

static int l_crypto_md5(lua_State *L)
{
    const char *inputData = luaL_checkstring(L,1);

    switch(lua_type(L, 2))
    {
        case LUA_TNUMBER:
        {
            int inputLen = luaL_checkinteger(L, 2);
            luaL_Buffer b;
            luaL_buffinit( L, &b );    
        
            memset(b.buffer,0,LUAL_BUFFERSIZE);
            aliyun_iot_common_md5(inputData, inputLen, b.p);
            b.p += strlen(b.buffer);
            
            luaL_pushresult( &b );
        }
        break;

        case LUA_TSTRING:
        {
            const char *pData;
            u32 sLen;
            
            pData = luaL_checklstring(L, 2, &sLen);
            if (strcmp(pData,"file")==0)
            {
                luaL_Buffer b;
                luaL_buffinit( L, &b );    
            
                memset(b.buffer,0,LUAL_BUFFERSIZE);
                aliyun_iot_common_file_md5(inputData, b.p);
                b.p += strlen(b.buffer);
                
                luaL_pushresult( &b );
            }
            else
            {
                luaL_error(L, "crypto.md5: the second parameter must be \"file\"");
            }
        }
        break;    
    default:
        return luaL_error(L, "crypto.md5: data must be number,string");
        break;
    }
    
    return 1;
}


static int l_crypto_hmac_sha1(lua_State *L)
{
    const char *inputData = luaL_checkstring(L,1);
    int inputLen = luaL_checkinteger(L, 2);
    const char *signKey = luaL_checkstring(L,3);
    int signKeyLen = luaL_checkinteger(L, 4);    

    luaL_Buffer b;
    luaL_buffinit( L, &b );    

    memset(b.buffer,0,LUAL_BUFFERSIZE);
    aliyun_iot_common_hmac_sha1(inputData, inputLen, b.p, signKey, signKeyLen);
    b.p += strlen(b.buffer);
    
    luaL_pushresult( &b );
    return 1;
}


static int l_crypto_sha1(lua_State *L)
{
    const char *inputData = luaL_checkstring(L,1);
    int inputLen = luaL_checkinteger(L, 2);

    luaL_Buffer b;
    luaL_buffinit( L, &b );    

    memset(b.buffer,0,LUAL_BUFFERSIZE);
    aliyun_iot_common_sha1(inputData, inputLen, b.p);
    b.p += strlen(b.buffer);
    
    luaL_pushresult( &b );
    return 1;
}

static int l_crypto_sha256(lua_State *L)
{
    int inputLen,i;
    const u8 *inputData = lua_tolstring(L,1,&inputLen);
    luaL_Buffer b;
    unsigned char out[32];
    luaL_buffinit( L, &b );    

    memset(b.buffer,0,LUAL_BUFFERSIZE);
    platform_crypto_sha256(inputData, inputLen, out);

    for (i = 0; i < 32; ++i) {
        b.buffer[i * 2] = l_iot_common_hb2hex(out[i] >> 4);
        b.buffer[i * 2 + 1] = l_iot_common_hb2hex(out[i]);
    }

    b.p += strlen(b.buffer);
    
    luaL_pushresult( &b );
    return 1;
}

static int l_crypto_hmac_sha2(lua_State *L)
{
    int inputLen,signKeyLen;
    const char *inputData = lua_tolstring( L, 1, &inputLen );
    const char *signKey = lua_tolstring(L, 2, &signKeyLen);
    unsigned char out[32];
    int i;

    luaL_Buffer b;
    luaL_buffinit( L, &b );    

    memset(b.buffer,0,LUAL_BUFFERSIZE);
    utils_hmac_sha256(inputData, inputLen, b.p, signKey, signKeyLen);
    b.p += strlen(b.buffer);
    
    luaL_pushresult( &b );

    return 1;
}


static int l_crypto_crc16(lua_State *L)
{   
    int inputLen;
    const char  *inputmethod = luaL_checkstring(L, 1);
    const u8 *inputData = lua_tolstring(L,2,&inputLen);
    u16 poly = luaL_optnumber(L,3,0x0000);
    u16 initial = luaL_optnumber(L,4,0x0000);
    u16 finally = luaL_optnumber(L,5,0x0000);
    u8 inReverse = luaL_optnumber(L,6,0);
    u8 outReverse = luaL_optnumber(L,7,0);
   
    lua_pushinteger(L, calcCRC16(inputData, inputmethod,inputLen,poly,initial,finally,inReverse,outReverse));
    return 1;
}

static int l_crypto_crc16_modbus(lua_State *L)
{
    const char *inputData = luaL_checkstring(L,1);
    int inputLen = luaL_checkinteger(L, 2);

    lua_pushinteger(L, calcCRC16_modbus(inputData, inputLen));
    return 1;
}

static int l_crypto_crc32(lua_State *L)
{
    const char *inputData = luaL_checkstring(L,1);
    int inputLen = luaL_checkinteger(L, 2);

    lua_pushinteger(L, calcCRC32(inputData, inputLen));
    return 1;
}

static int l_crypto_crc8(lua_State *L)
{
    const char *inputData = luaL_checkstring(L,1);
    int inputLen = luaL_checkinteger(L, 2);

    lua_pushinteger(L, calcCRC8(inputData, inputLen));
    return 1;
}

#define AES_BLOCK_LEN 16

static void DeletePaddingBuf(luaL_Buffer *B, u8 *pPadding, size_t nBufLen, u8 *pBuf, u8 nBlockLen)
{
    u8 nPadLen;
    if((strcmp(pPadding, "PKCS5")==0) || (strcmp(pPadding, "PKCS7")==0))
    {
        nPadLen = *(pBuf+nBufLen-1);
        //printf("aes DeletePaddingBuf length=%d\n", nPadLen);
        if((nBlockLen-nPadLen) >= 0)
        {
            luaL_addlstring(B, pBuf, nBufLen-nPadLen);
        }
    }
    else if(strcmp(pPadding, "ZERO")==0)
    {                        
        u8 *pEnd = pBuf+nBufLen-1;
        nPadLen = 0;
        while(1)
        {
            if(*pEnd == 0)
            {
                nPadLen++;
                if(nPadLen == nBlockLen)
                {
                    break;
                }
                pEnd--;
            }
            else
            {
                break;
            }
        }
        //printf("aes DeletePaddingBuf length=%d\n", nPadLen);
        if((nBlockLen-nPadLen) >= 0)
        {
            luaL_addlstring(B, pBuf, nBufLen-nPadLen);
        }
    }
    else
    {
        luaL_addlstring(B, pBuf, nBufLen);
    }
}


static int l_crypto_aes128_ecb_encrypt(lua_State *L)
{
    size_t nBufLen = 0;
    u8 *pBuf = lua_tolstring(L, 1, &nBufLen);
    size_t nPswdLen = 0;
    u8 *pPassword = lua_tolstring(L, 3, &nPswdLen);

    int nPadLen = AES_BLOCK_LEN-(nBufLen%AES_BLOCK_LEN);
    u8 pPadBuf[AES_BLOCK_LEN];
    u8 *pInBuf = NULL;

    //检查参数合法性
    if(nPswdLen!=16)
    {
        return luaL_error(L, "invalid password length=%d, only support AES128", nPswdLen);
    }
   
    //构造填充数据
    memset(pPadBuf, 0, sizeof(pPadBuf));
    
    //加密
    {       
        luaL_Buffer b;
        u32 nRmnLen;
        memset(b.buffer,0,LUAL_BUFFERSIZE);
        luaL_buffinit( L, &b );

         //原始数据和填充数据拼接在一起
        pInBuf = malloc(nBufLen+nPadLen);
        if(pInBuf == NULL)
        {
            printf("aes_encrypt malloc error!!!\n");
            luaL_pushresult( &b );
            return 1;
        }
        memcpy(pInBuf, pBuf, nBufLen);
        memcpy(pInBuf+nBufLen, pPadBuf, nPadLen); 
        nBufLen += nPadLen;
        nRmnLen = nBufLen;
       
        //开始分组加密，每16字节一组
        while(nRmnLen>0)
        {
            AES_Encrypt("ECB", nRmnLen, pInBuf+nBufLen-nRmnLen, nPswdLen, pPassword, "");
            luaL_addlstring(&b, pInBuf+nBufLen-nRmnLen, AES_BLOCK_LEN);
            nRmnLen -= AES_BLOCK_LEN;
        }

        luaL_pushresult( &b );
        return 1;
    }    
}


static int l_crypto_aes128_ecb_decrypt(lua_State *L)
{    
    size_t nBufLen = 0;
    u8 *pBuf = lua_tolstring(L, 1, &nBufLen);
    size_t nPswdLen = 0;
    u8 *pPassword = lua_tolstring(L, 3, &nPswdLen);

    //检查参数合法性
    if(nPswdLen!=16)
    {
        return luaL_error(L, "invalid password length=%d, only support AES128", nPswdLen);
    }
    
    
    //解密
    {       
        luaL_Buffer b;
        u32 nRmnLen;
        memset(b.buffer,0,LUAL_BUFFERSIZE);
        luaL_buffinit( L, &b );

        nRmnLen = nBufLen;

        //开始分组解密，每16字节一组
        while(nRmnLen>0)
        {
            AES_Decrypt("ECB", nRmnLen, pBuf+nBufLen-nRmnLen, nPswdLen, pPassword, "");

            //删除填充数据
            if(nRmnLen==AES_BLOCK_LEN)
            {
                DeletePaddingBuf(&b, "ZERO", AES_BLOCK_LEN, pBuf+nBufLen-nRmnLen, AES_BLOCK_LEN);
            }
            else
            {
                luaL_addlstring(&b, pBuf+nBufLen-nRmnLen, AES_BLOCK_LEN);
            }
            nRmnLen -= AES_BLOCK_LEN;
        }


        luaL_pushresult( &b );
        return 1;
    }    
}

static int l_crypto_aes_encrypt(lua_State *L)
{    
    u8 *pMode = luaL_checkstring(L, 1);
    u8 *pPadding = luaL_checkstring(L, 2);
    size_t nBufLen = 0;
    u8 *pBuf = lua_tolstring(L, 3, &nBufLen);
    size_t nPswdLen = 0;
    u8 *pPassword = lua_tolstring(L, 4, &nPswdLen);
    size_t nIVLen = 0;
    u8 *pIV =  lua_tolstring(L, 5, &nIVLen);

    int nPadLen = AES_BLOCK_LEN-(nBufLen%AES_BLOCK_LEN);
    u8 pPadBuf[AES_BLOCK_LEN];
    u8 *pInBuf = NULL;

    //检查参数合法性
    if((nPswdLen!=16) && (nPswdLen!=24) && (nPswdLen!=32))
    {
        return luaL_error(L, "invalid password length=%d, only support AES128,AES192,AES256", nPswdLen);
    }
    if((strcmp(pMode, "ECB")!=0) && (strcmp(pMode, "CBC")!=0) && (strcmp(pMode, "CTR")!=0))
    {
        return luaL_error(L, "invalid mode=%s, only support ECB,CBC,CTR", pMode);
    }
    if((strcmp(pPadding, "NONE")!=0) && (strcmp(pPadding, "PKCS5")!=0) && (strcmp(pPadding, "PKCS7")!=0) && (strcmp(pPadding, "ZERO")!=0))
    {
        return luaL_error(L, "invalid padding=%s, only support NONE,PKCS5,PKCS7,ZERO", pPadding);
    }
    if(((strcmp(pMode, "CBC")==0) || (strcmp(pMode, "CTR")==0)) && (nIVLen!=16))
    {
        return luaL_error(L, "invalid iv length=%d, only support 16", nIVLen);
    }
    
    //构造填充数据
    if((strcmp(pPadding, "PKCS5")==0) || (strcmp(pPadding, "PKCS7")==0))
    {
        memset(pPadBuf, nPadLen, sizeof(pPadBuf));
    }
    else if(strcmp(pPadding, "ZERO")==0)
    {
        memset(pPadBuf, 0, sizeof(pPadBuf));
    }   
    
    //加密
    {       
        luaL_Buffer b;
        u32 nRmnLen;
        memset(b.buffer,0,LUAL_BUFFERSIZE);
        luaL_buffinit( L, &b );

         //原始数据和填充数据拼接在一起
        pInBuf = malloc(nBufLen+nPadLen);
        if(pInBuf == NULL)
        {
            printf("aes_encrypt malloc error!!!\n");
            luaL_pushresult( &b );
            return 1;
        }
        memcpy(pInBuf, pBuf, nBufLen);
        memcpy(pInBuf+nBufLen, pPadBuf, nPadLen); 
        nBufLen += nPadLen;
        nRmnLen = nBufLen;

        if(strcmp(pMode, "ECB") == 0)
        {
            //开始分组加密，每16字节一组
            while(nRmnLen>0)
            {
                AES_Encrypt(pMode, nRmnLen, pInBuf+nBufLen-nRmnLen, nPswdLen, pPassword, pIV);
                luaL_addlstring(&b, pInBuf+nBufLen-nRmnLen, AES_BLOCK_LEN);
                nRmnLen -= AES_BLOCK_LEN;
            }
        }
        else if((strcmp(pMode, "CBC") == 0) || (strcmp(pMode, "CTR") == 0))
        {
            //待加密数据一次性传入
            AES_Encrypt(pMode, nBufLen, pInBuf, nPswdLen, pPassword, pIV);
            luaL_addlstring(&b, pInBuf, nBufLen);
        }

        luaL_pushresult( &b );
        return 1;
    }    
}


static int l_crypto_aes_decrypt(lua_State *L)
{    
    u8 *pMode = luaL_checkstring(L, 1);
    u8 *pPadding = luaL_checkstring(L, 2);
    size_t nBufLen = 0;
    u8 *pBuf = lua_tolstring(L, 3, &nBufLen);
    size_t nPswdLen = 0;
    u8 *pPassword = lua_tolstring(L, 4, &nPswdLen);
    size_t nIVLen = 0;
    u8 *pIV =  lua_tolstring(L, 5, &nIVLen);

    //检查参数合法性
    if((nPswdLen!=16) && (nPswdLen!=24) && (nPswdLen!=32))
    {
        return luaL_error(L, "invalid password length=%d, only support AES128,AES192,AES256", nPswdLen);
    }
    if((strcmp(pMode, "ECB")!=0) && (strcmp(pMode, "CBC")!=0) && (strcmp(pMode, "CTR")!=0))
    {
        return luaL_error(L, "invalid mode=%s, only support ECB,CBC,CTR", pMode);
    }
    if((strcmp(pPadding, "NONE")!=0) && (strcmp(pPadding, "PKCS5")!=0) && (strcmp(pPadding, "PKCS7")!=0) && (strcmp(pPadding, "ZERO")!=0))
    {
        return luaL_error(L, "invalid padding=%s, only support NONE,PKCS5,PKCS7,ZERO", pPadding);
    }
    if(((strcmp(pMode, "CBC")==0) || (strcmp(pMode, "CTR")==0)) && (nIVLen!=16))
    {
        return luaL_error(L, "invalid iv length=%d, only support 16", nIVLen);
    }    
    
    
    //解密
    {       
        luaL_Buffer b;
        u32 nRmnLen;
        memset(b.buffer,0,LUAL_BUFFERSIZE);
        luaL_buffinit( L, &b );

        nRmnLen = nBufLen;

        if(strcmp(pMode, "ECB") == 0)
        {
            //开始分组解密，每16字节一组
            while(nRmnLen>0)
            {
                AES_Decrypt(pMode, nRmnLen, pBuf+nBufLen-nRmnLen, nPswdLen, pPassword, pIV);

                //删除填充数据
                if(nRmnLen==AES_BLOCK_LEN)
                {
                    DeletePaddingBuf(&b, pPadding, AES_BLOCK_LEN, pBuf+nBufLen-nRmnLen, AES_BLOCK_LEN);
                }
                else
                {
                    luaL_addlstring(&b, pBuf+nBufLen-nRmnLen, AES_BLOCK_LEN);
                }
                nRmnLen -= AES_BLOCK_LEN;
            }
        }
        else if((strcmp(pMode, "CBC") == 0) || (strcmp(pMode, "CTR") == 0))
        {
            //待解密数据一次性传入
            AES_Decrypt(pMode, nBufLen, pBuf, nPswdLen, pPassword, pIV);
            DeletePaddingBuf(&b, pPadding, nBufLen, pBuf, AES_BLOCK_LEN);
        }

        luaL_pushresult( &b );
        return 1;
    }    
}

#define DES_BLOCK_LEN (8)
static int l_crypto_des_encrypt(lua_State *L)
{    
    u8 *pMode = luaL_checkstring(L, 1);
    u8 *pPadding = luaL_checkstring(L, 2);
    size_t nBufLen = 0;
    u8 *pBuf = lua_tolstring(L, 3, &nBufLen);
    size_t nPswdLen = 0;
    u8 *pPassword = lua_tolstring(L, 4, &nPswdLen);
    size_t nIVLen = 0;
    u8 *pIV =  lua_tolstring(L, 5, &nIVLen);

    int nPadLen = DES_BLOCK_LEN-(nBufLen%DES_BLOCK_LEN);
    u8 pPadBuf[DES_BLOCK_LEN];
    u8 *pInBuf = NULL;

    //检查参数合法性
    if((nPswdLen!=8))
    {
        return luaL_error(L, "invalid password length=%d, only support 8 bytes", nPswdLen);
    }
    if((strcmp(pMode, "ECB")!=0) && (strcmp(pMode, "CBC")!=0))
    {
        return luaL_error(L, "invalid mode=%s, only support ECB,CBC", pMode);
    }
    if((strcmp(pPadding, "NONE")!=0) && (strcmp(pPadding, "PKCS5")!=0) && (strcmp(pPadding, "PKCS7")!=0) && (strcmp(pPadding, "ZERO")!=0))
    {
        return luaL_error(L, "invalid padding=%s, only support NONE,PKCS5,PKCS7,ZERO", pPadding);
    }
    if((strcmp(pMode, "CBC")==0) && (nIVLen!=8))
    {
        return luaL_error(L, "invalid iv length=%d, only support 8 bytes", nIVLen);
    }
    
    //构造填充数据
    if((strcmp(pPadding, "PKCS5")==0) || (strcmp(pPadding, "PKCS7")==0))
    {
        memset(pPadBuf, nPadLen, sizeof(pPadBuf));
    }
    else if(strcmp(pPadding, "ZERO")==0)
    {
        memset(pPadBuf, 0, sizeof(pPadBuf));
    }   
    else if(strcmp(pPadding, "NONE")==0)
    {
        if(nBufLen%DES_BLOCK_LEN != 0)
        {
            return luaL_error(L, "buf len should be multiple of 8, len=%d", nBufLen);
        }
        nPadLen = 0;
    }
    
    //加密
    {       
        luaL_Buffer b;
        u32 nRmnLen;
        memset(b.buffer,0,LUAL_BUFFERSIZE);
        luaL_buffinit( L, &b );

         //原始数据和填充数据拼接在一起
        pInBuf = malloc(nBufLen+nPadLen);
        if(pInBuf == NULL)
        {
            printf("des_encrypt malloc error!!!\n");
            luaL_pushresult( &b );
            return 1;
        }
        memcpy(pInBuf, pBuf, nBufLen);
        if(nPadLen != 0)
        {
            memcpy(pInBuf+nBufLen, pPadBuf, nPadLen); 
            nBufLen += nPadLen;
        }
        nRmnLen = nBufLen;

        am_crypto_mbedtls_des_context ctx;
        am_crypto_mbedtls_des_init( &ctx );
        am_crypto_mbedtls_des_setkey_enc( &ctx, pPassword );

        if(strcmp(pMode, "ECB") == 0)
        {
            //开始分组加密，每8字节一组
            while(nRmnLen>0)
            {
                //am_crypto_mbedtls_des_setkey_enc( &ctx, pPassword );
                am_crypto_mbedtls_des_crypt_ecb( &ctx, pInBuf+nBufLen-nRmnLen, pInBuf+nBufLen-nRmnLen );
                luaL_addlstring(&b, pInBuf+nBufLen-nRmnLen, DES_BLOCK_LEN);
                nRmnLen -= DES_BLOCK_LEN;
            }
        }
        else if(strcmp(pMode, "CBC") == 0)
        {
            //待加密数据一次性传入
            am_crypto_mbedtls_des_crypt_cbc( &ctx, AM_CRYPTO_MBEDTLS_DES_ENCRYPT, nBufLen, pIV, pInBuf, pInBuf );
            luaL_addlstring(&b, pInBuf, nBufLen);
        }

        am_crypto_mbedtls_des_free( &ctx );

        luaL_pushresult( &b );
        return 1;
    }    
}


static int l_crypto_des_decrypt(lua_State *L)
{    
    u8 *pMode = luaL_checkstring(L, 1);
    u8 *pPadding = luaL_checkstring(L, 2);
    size_t nBufLen = 0;
    u8 *pBuf = lua_tolstring(L, 3, &nBufLen);
    size_t nPswdLen = 0;
    u8 *pPassword = lua_tolstring(L, 4, &nPswdLen);
    size_t nIVLen = 0;
    u8 *pIV =  lua_tolstring(L, 5, &nIVLen);

    //检查参数合法性
    if(nPswdLen!=8)
    {
        return luaL_error(L, "invalid password length=%d, only support 8 bytes", nPswdLen);
    }
    if((strcmp(pMode, "ECB")!=0) && (strcmp(pMode, "CBC")!=0))
    {
        return luaL_error(L, "invalid mode=%s, only support ECB,CBC", pMode);
    }
    if((strcmp(pPadding, "NONE")!=0) && (strcmp(pPadding, "PKCS5")!=0) && (strcmp(pPadding, "PKCS7")!=0) && (strcmp(pPadding, "ZERO")!=0))
    {
        return luaL_error(L, "invalid padding=%s, only support NONE,PKCS5,PKCS7,ZERO", pPadding);
    }
    if((strcmp(pMode, "CBC")==0) && (nIVLen!=8))
    {
        return luaL_error(L, "invalid iv length=%d, only support 8 bytes", nIVLen);
    }   
    
    
    //解密
    {       
        luaL_Buffer b;
        u32 nRmnLen;
        memset(b.buffer,0,LUAL_BUFFERSIZE);
        luaL_buffinit( L, &b );

        nRmnLen = nBufLen;

        am_crypto_mbedtls_des_context ctx;
        am_crypto_mbedtls_des_init( &ctx );
        am_crypto_mbedtls_des_setkey_dec( &ctx, pPassword );

        if(strcmp(pMode, "ECB") == 0)
        {
            //开始分组解密，每8字节一组
            while(nRmnLen>0)
            {
                //am_crypto_mbedtls_des_setkey_dec( &ctx, pPassword );
                am_crypto_mbedtls_des_crypt_ecb( &ctx, pBuf+nBufLen-nRmnLen, pBuf+nBufLen-nRmnLen );

                //删除填充数据
                if(nRmnLen==DES_BLOCK_LEN)
                {
                    DeletePaddingBuf(&b, pPadding, DES_BLOCK_LEN, pBuf+nBufLen-nRmnLen, DES_BLOCK_LEN);
                }
                else
                {
                    luaL_addlstring(&b, pBuf+nBufLen-nRmnLen, DES_BLOCK_LEN);
                }
                nRmnLen -= DES_BLOCK_LEN;
            }
        }
        else if(strcmp(pMode, "CBC") == 0)
        {
            //待解密数据一次性传入
            am_crypto_mbedtls_des_crypt_cbc( &ctx, AM_CRYPTO_MBEDTLS_DES_DECRYPT, nBufLen, pIV, pBuf, pBuf );
            DeletePaddingBuf(&b, pPadding, nBufLen, pBuf, DES_BLOCK_LEN);
        }

        luaL_pushresult( &b );
        return 1;
    }    
}



#if 1//def AM_XXTEA_SUPPORT 
#include "xxtea.h"
static int l_crypto_xxtea_encrypt(lua_State *L) {
    unsigned char *result;
    const char *data, *key;
    size_t data_len, key_len, out_len;
    u8 *pPadding;

    data = luaL_checklstring(L, 1, &data_len);
    key  = luaL_checklstring(L, 2, &key_len);
    pPadding = luaL_optstring(L, 3, "ZERO_LEN");

    //检查参数合法性
    if((strcmp(pPadding, "NONE")!=0) && (strcmp(pPadding, "ZERO_LEN")!=0))
    {
        return luaL_error(L, "l_crypto_xxtea_encrypt invalid padding=%s, only support NONE,ZERO_LEN", pPadding);
    }
    if((strcmp(pPadding, "NONE")==0))
    {
        if((data_len&3) != 0)
        {
            return luaL_error(L, "l_crypto_xxtea_encrypt invalid data_len=%d, only support 4N", data_len);
        }
    }
    
    result = xxtea_encrypt(data, data_len, key, &out_len, (strcmp(pPadding, "ZERO_LEN")==0) ? 1 : 0);
    
    if(result == NULL)
    {
        lua_pushnil(L);
    }
    else
    {
        lua_pushlstring(L, (const char *)result, out_len);
        free(result);
    }
    
    return 1;
}

static int l_crypto_xxtea_decrypt(lua_State *L) {
    unsigned char *result;
    const char *data, *key;
    size_t data_len, key_len, out_len;
    u8 *pPadding;
    
    data = luaL_checklstring(L, 1, &data_len);
    key  = luaL_checklstring(L, 2, &key_len);
    pPadding = luaL_optstring(L, 3, "ZERO_LEN");

    //检查参数合法性
    if((strcmp(pPadding, "NONE")!=0) && (strcmp(pPadding, "ZERO_LEN")!=0))
    {
        return luaL_error(L, "l_crypto_xxtea_encrypt invalid padding=%s, only support NONE,ZERO_LEN", pPadding);
    }
	
    result = xxtea_decrypt(data, data_len, key, &out_len, (strcmp(pPadding, "ZERO_LEN")==0) ? 1 : 0);
    
    if(result == NULL)
    {
        lua_pushnil(L);
    }
    else
    {
        lua_pushlstring(L, (const char *)result, out_len);
        free(result);
    }
    
    return 1;
}
#endif




#define MIN_OPT_LEVEL 2
#include "lrodefs.h"
const LUA_REG_TYPE crypto_map[] =
{
    { LSTRKEY( "base64_encode" ),  LFUNCVAL( l_crypto_base64_encode ) },
    { LSTRKEY( "base64_decode" ),  LFUNCVAL( l_crypto_base64_decode ) },
    { LSTRKEY( "hmac_md5" ),  LFUNCVAL( l_crypto_hmac_md5 ) },
    { LSTRKEY( "md5" ),  LFUNCVAL( l_crypto_md5 ) },
    { LSTRKEY( "hmac_sha1" ),  LFUNCVAL( l_crypto_hmac_sha1 ) },
    { LSTRKEY( "sha1" ),  LFUNCVAL( l_crypto_sha1 ) },
    { LSTRKEY( "sha256" ),  LFUNCVAL( l_crypto_sha256 ) },
    { LSTRKEY( "hmac_sha256" ),  LFUNCVAL( l_crypto_hmac_sha2 ) },
    { LSTRKEY( "crc16" ),  LFUNCVAL( l_crypto_crc16 ) },
    { LSTRKEY( "crc16_modbus" ),  LFUNCVAL( l_crypto_crc16_modbus ) },
    { LSTRKEY( "crc32" ),  LFUNCVAL( l_crypto_crc32 ) },
    { LSTRKEY( "crc8" ),  LFUNCVAL( l_crypto_crc8 ) },
    { LSTRKEY( "aes128_ecb_encrypt" ),  LFUNCVAL( l_crypto_aes128_ecb_encrypt ) },
    { LSTRKEY( "aes128_ecb_decrypt" ),  LFUNCVAL( l_crypto_aes128_ecb_decrypt ) },
    { LSTRKEY( "aes_encrypt" ),  LFUNCVAL( l_crypto_aes_encrypt ) },
    { LSTRKEY( "aes_decrypt" ),  LFUNCVAL( l_crypto_aes_decrypt ) },
    { LSTRKEY( "des_encrypt" ),  LFUNCVAL( l_crypto_des_encrypt ) },
    { LSTRKEY( "des_decrypt" ),  LFUNCVAL( l_crypto_des_decrypt ) },
    #if 1//def AM_XXTEA_SUPPORT
    { LSTRKEY( "xxtea_encrypt" ),  LFUNCVAL( l_crypto_xxtea_encrypt ) },
    { LSTRKEY( "xxtea_decrypt" ),  LFUNCVAL( l_crypto_xxtea_decrypt ) },
    #endif

    { LNILKEY, LNILVAL }
};

const LUA_REG_TYPE crypto_fwmd5_map[] =
{
    { LSTRKEY( "flow_md5" ),  LFUNCVAL( l_crypto_flow_md5 ) },


    { LNILKEY, LNILVAL }
};
const LUA_REG_TYPE crypto_fwmd5_mt_map[] =
{
    { LSTRKEY( "update" ),  LFUNCVAL( l_crypto_md5_update ) },
    { LSTRKEY( "hexdigest" ),  LFUNCVAL( l_crypto_md5_hexdigest ) },

    { LNILKEY, LNILVAL }
};

int luaopen_crypto( lua_State *L )
{
    luaL_register( L, AUXLIB_CRYPTO, crypto_map );

    luaL_newmetatable( L, META_NAME );
    lua_pushvalue(L,-1);
    lua_setfield(L, -2, "__index");
    luaL_register( L, NULL, crypto_fwmd5_mt_map );
    luaL_register( L, AUXLIB_CRYPTO, crypto_fwmd5_map );
    return 1;
}

