/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2016        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "platform.h"
#include "auxmods.h"
#include "lrotable.h"
#include "common.h"
#include "sermux.h"
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include "platform_conf.h"
#include "platform_spi.h"
#include "platform_audio.h"
#include "lrodefs.h"


#include "ff.h"			/* Obtains integer types */
#include "diskio.h"		/* Declarations of disk functions */


/*--------------------------------------------------------------------------

   Module Private Functions

---------------------------------------------------------------------------*/

/* MMC/SD command (SPI mode) */
#define CMD0	(0)			/* GO_IDLE_STATE */
#define CMD1	(1)			/* SEND_OP_COND */
#define	ACMD41	(0x80+41)	/* SEND_OP_COND (SDC) */
#define CMD8	(8)			/* SEND_IF_COND */
#define CMD9	(9)			/* SEND_CSD */
#define CMD10	(10)		/* SEND_CID */
#define CMD12	(12)		/* STOP_TRANSMISSION */
#define CMD13	(13)		/* SEND_STATUS */
#define ACMD13	(0x80+13)	/* SD_STATUS (SDC) */
#define CMD16	(16)		/* SET_BLOCKLEN */
#define CMD17	(17)		/* READ_SINGLE_BLOCK */
#define CMD18	(18)		/* READ_MULTIPLE_BLOCK */
#define CMD23	(23)		/* SET_BLOCK_COUNT */
#define	ACMD23	(0x80+23)	/* SET_WR_BLK_ERASE_COUNT (SDC) */
#define CMD24	(24)		/* WRITE_BLOCK */
#define CMD25	(25)		/* WRITE_MULTIPLE_BLOCK */
#define CMD32	(32)		/* ERASE_ER_BLK_START */
#define CMD33	(33)		/* ERASE_ER_BLK_END */
#define CMD38	(38)		/* ERASE */
#define CMD55	(55)		/* APP_CMD */
#define CMD58	(58)		/* READ_OCR */


static
DSTATUS Stat = STA_NOINIT;	/* Disk status */

static
BYTE CardType;			/* b0:MMC, b1:SDv1, b2:SDv2, b3:Block addressing */

static
BYTE FATFS_DEBUG = 0; // debug log, 0 -- disable , 1 -- enable

static
BYTE FATFS_SPI_ID = 0; // 0 -- SPI_1, 1 -- SPI_2
static
BYTE FATFS_SPI_CS = 3; // GPIO 3

static void dly_us(BYTE us) {
	if (us < 1) {
		return;
	}
	us += 999;
	platform_os_sleep(us/1000);
}

/*-----------------------------------------------------------------------*/
/* Transmit bytes to the card (bitbanging)                               */
/*-----------------------------------------------------------------------*/

static
void xmit_mmc (
	const BYTE* buff,	/* Data to be sent */
	UINT bc				/* Number of bytes to send */
)
{
	#if 0
	BYTE d;


	do {
		d = *buff++;	/* Get a byte to be sent */
		if (d & 0x80) DI_H(); else DI_L();	/* bit7 */
		CK_H(); CK_L();
		if (d & 0x40) DI_H(); else DI_L();	/* bit6 */
		CK_H(); CK_L();
		if (d & 0x20) DI_H(); else DI_L();	/* bit5 */
		CK_H(); CK_L();
		if (d & 0x10) DI_H(); else DI_L();	/* bit4 */
		CK_H(); CK_L();
		if (d & 0x08) DI_H(); else DI_L();	/* bit3 */
		CK_H(); CK_L();
		if (d & 0x04) DI_H(); else DI_L();	/* bit2 */
		CK_H(); CK_L();
		if (d & 0x02) DI_H(); else DI_L();	/* bit1 */
		CK_H(); CK_L();
		if (d & 0x01) DI_H(); else DI_L();	/* bit0 */
		CK_H(); CK_L();
	} while (--bc);
	#endif
	if (FATFS_DEBUG)
		printf("[FatFS]xmit_mmc bc=%d\r\n", bc);
	platform_spi_send(FATFS_SPI_ID, buff, bc);
}



/*-----------------------------------------------------------------------*/
/* Receive bytes from the card (bitbanging)                              */
/*-----------------------------------------------------------------------*/

static
void rcvr_mmc (
	BYTE *buff,	/* Pointer to read buffer */
	UINT bc		/* Number of bytes to receive */
)
{
	#if 0
	BYTE r;


	DI_H();	/* Send 0xFF */

	do {
		r = 0;	 if (DO) r++;	/* bit7 */
		CK_H(); CK_L();
		r <<= 1; if (DO) r++;	/* bit6 */
		CK_H(); CK_L();
		r <<= 1; if (DO) r++;	/* bit5 */
		CK_H(); CK_L();
		r <<= 1; if (DO) r++;	/* bit4 */
		CK_H(); CK_L();
		r <<= 1; if (DO) r++;	/* bit3 */
		CK_H(); CK_L();
		r <<= 1; if (DO) r++;	/* bit2 */
		CK_H(); CK_L();
		r <<= 1; if (DO) r++;	/* bit1 */
		CK_H(); CK_L();
		r <<= 1; if (DO) r++;	/* bit0 */
		CK_H(); CK_L();
		*buff++ = r;			/* Store a received byte */
	} while (--bc);
	#endif
	u8* buf2 = 0x00;
	u8** buf = &buf2;
	u8 tmp[bc];
	
	for(size_t i = 0; i < bc; i++)
	{
		tmp[i] = 0xFF;
	}
	
	s32 t = platform_spi_send_recv(FATFS_SPI_ID, tmp, buf, bc);
	//s32 t = platform_spi_recv(0, buf, bc);
	
	memcpy(buff, buf2, bc);
	if (FATFS_DEBUG)
		printf("[FatFS]rcvr_mmc first resp byte=%02X, t=%d\r\n", buf2[0], t);
	free(buf2);
}



/*-----------------------------------------------------------------------*/
/* Wait for card ready                                                   */
/*-----------------------------------------------------------------------*/

static
int wait_ready (void)	/* 1:OK, 0:Timeout */
{
	BYTE d;
	UINT tmr;


	for (tmr = 5000; tmr; tmr--) {	/* Wait for ready in timeout of 500ms */
		rcvr_mmc(&d, 1);
		if (d == 0xFF) break;
		dly_us(100);
	}

	return tmr ? 1 : 0;
}



/*-----------------------------------------------------------------------*/
/* Deselect the card and release SPI bus                                 */
/*-----------------------------------------------------------------------*/

static
void deselect (void)
{
	BYTE d;

	//CS_H();				/* Set CS# high */
	platform_pio_op(0, 1 << FATFS_SPI_CS, 0);
	rcvr_mmc(&d, 1);	/* Dummy clock (force DO hi-z for multiple slave SPI) */
}



/*-----------------------------------------------------------------------*/
/* Select the card and wait for ready                                    */
/*-----------------------------------------------------------------------*/

static
int select (void)	/* 1:OK, 0:Timeout */
{
	BYTE d;

	//CS_L();				/* Set CS# low */
	platform_pio_op(0, 1 << FATFS_SPI_CS, 1);
	rcvr_mmc(&d, 1);	/* Dummy clock (force DO enabled) */
	if (wait_ready()) return 1;	/* Wait for card ready */

	deselect();
	return 0;			/* Failed */
}



/*-----------------------------------------------------------------------*/
/* Receive a data packet from the card                                   */
/*-----------------------------------------------------------------------*/

static
int rcvr_datablock (	/* 1:OK, 0:Failed */
	BYTE *buff,			/* Data buffer to store received data */
	UINT btr			/* Byte count */
)
{
	BYTE d[2];
	UINT tmr;


	for (tmr = 1000; tmr; tmr--) {	/* Wait for data packet in timeout of 100ms */
		rcvr_mmc(d, 1);
		if (d[0] != 0xFF) break;
		dly_us(100);
	}
	if (d[0] != 0xFE) return 0;		/* If not valid data token, return with error */

	rcvr_mmc(buff, btr);			/* Receive the data block into buffer */
	rcvr_mmc(d, 2);					/* Discard CRC */

	return 1;						/* Return with success */
}



/*-----------------------------------------------------------------------*/
/* Send a data packet to the card                                        */
/*-----------------------------------------------------------------------*/

static
int xmit_datablock (	/* 1:OK, 0:Failed */
	const BYTE *buff,	/* 512 byte data block to be transmitted */
	BYTE token			/* Data/Stop token */
)
{
	BYTE d[2];


	if (!wait_ready()) return 0;

	d[0] = token;
	xmit_mmc(d, 1);				/* Xmit a token */
	if (token != 0xFD) {		/* Is it data token? */
		xmit_mmc(buff, 512);	/* Xmit the 512 byte data block to MMC */
		rcvr_mmc(d, 2);			/* Xmit dummy CRC (0xFF,0xFF) */
		rcvr_mmc(d, 1);			/* Receive data response */
		if ((d[0] & 0x1F) != 0x05)	/* If not accepted, return with error */
			return 0;
	}

	return 1;
}



/*-----------------------------------------------------------------------*/
/* Send a command packet to the card                                     */
/*-----------------------------------------------------------------------*/

static
BYTE send_cmd (		/* Returns command response (bit7==1:Send failed)*/
	BYTE cmd,		/* Command byte */
	DWORD arg		/* Argument */
)
{
	BYTE n, d, buf[6];


	if (cmd & 0x80) {	/* ACMD<n> is the command sequense of CMD55-CMD<n> */
		cmd &= 0x7F;
		n = send_cmd(CMD55, 0);
		if (n > 1) return n;
	}

	/* Select the card and wait for ready except to stop multiple block read */
	if (cmd != CMD12) {
		deselect();
		if (!select()) return 0xFF;
	}

	/* Send a command packet */
	buf[0] = 0x40 | cmd;			/* Start + Command index */
	buf[1] = (BYTE)(arg >> 24);		/* Argument[31..24] */
	buf[2] = (BYTE)(arg >> 16);		/* Argument[23..16] */
	buf[3] = (BYTE)(arg >> 8);		/* Argument[15..8] */
	buf[4] = (BYTE)arg;				/* Argument[7..0] */
	n = 0x01;						/* Dummy CRC + Stop */
	if (cmd == CMD0) n = 0x95;		/* (valid CRC for CMD0(0)) */
	if (cmd == CMD8) n = 0x87;		/* (valid CRC for CMD8(0x1AA)) */
	buf[5] = n;
	xmit_mmc(buf, 6);

	/* Receive command response */
	if (cmd == CMD12) rcvr_mmc(&d, 1);	/* Skip a stuff byte when stop reading */
	n = 10;								/* Wait for a valid response in timeout of 10 attempts */
	do
		rcvr_mmc(&d, 1);
	while ((d & 0x80) && --n);

	return d;			/* Return with the response value */
}



/*--------------------------------------------------------------------------

   Public Functions

---------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------*/
/* Get Disk Status                                                       */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status (
	BYTE drv			/* Drive number (always 0) */
)
{
	if (drv) return STA_NOINIT;

	return Stat;
}



/*-----------------------------------------------------------------------*/
/* Initialize Disk Drive                                                 */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize (
	BYTE drv		/* Physical drive nmuber (0) */
)
{
	BYTE n, ty, cmd, buf[4];
	UINT tmr;
	DSTATUS s;


	if (drv) return RES_NOTRDY;

	dly_us(10000);			/* 10ms */
	//CS_INIT(); CS_H();		/* Initialize port pin tied to CS */
	//CK_INIT(); CK_L();		/* Initialize port pin tied to SCLK */
	//DI_INIT();				/* Initialize port pin tied to DI */
	//DO_INIT();				/* Initialize port pin tied to DO */

	platform_spi_close(FATFS_SPI_ID);
	platform_spi_setup(FATFS_SPI_ID, 400*1000/*400khz*/, 0, 0, 8, 1, 1);

	for (n = 10; n; n--) rcvr_mmc(buf, 1);	/* Apply 80 dummy clocks and the card gets ready to receive command */

	ty = 0;
	if (send_cmd(CMD0, 0) == 1) {			/* Enter Idle state */
		if (send_cmd(CMD8, 0x1AA) == 1) {	/* SDv2? */
			rcvr_mmc(buf, 4);							/* Get trailing return value of R7 resp */
			if (buf[2] == 0x01 && buf[3] == 0xAA) {		/* The card can work at vdd range of 2.7-3.6V */
				for (tmr = 1000; tmr; tmr--) {			/* Wait for leaving idle state (ACMD41 with HCS bit) */
					if (send_cmd(ACMD41, 1UL << 30) == 0) break;
					dly_us(1000);
				}
				if (tmr && send_cmd(CMD58, 0) == 0) {	/* Check CCS bit in the OCR */
					rcvr_mmc(buf, 4);
					ty = (buf[0] & 0x40) ? CT_SD2 | CT_BLOCK : CT_SD2;	/* SDv2 */
				}
			}
		} else {							/* SDv1 or MMCv3 */
			if (send_cmd(ACMD41, 0) <= 1) 	{
				ty = CT_SD1; cmd = ACMD41;	/* SDv1 */
			} else {
				ty = CT_MMC; cmd = CMD1;	/* MMCv3 */
			}
			for (tmr = 1000; tmr; tmr--) {			/* Wait for leaving idle state */
				if (send_cmd(cmd, 0) == 0) break;
				dly_us(1000);
			}
			if (!tmr || send_cmd(CMD16, 512) != 0)	/* Set R/W block length to 512 */
				ty = 0;
		}
	}
	CardType = ty;
	s = ty ? 0 : STA_NOINIT;
	Stat = s;

	deselect();

	platform_spi_close(FATFS_SPI_ID);
	//spi.setup(id,0,0,8,400*1000,1)
	platform_spi_setup(FATFS_SPI_ID, 1000*1000/*1Mhz*/, 0, 0, 8, 1, 1);

	return s;
}



/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read (
	BYTE drv,			/* Physical drive nmuber (0) */
	BYTE *buff,			/* Pointer to the data buffer to store read data */
	DWORD sector,		/* Start sector number (LBA) */
	UINT count			/* Sector count (1..128) */
)
{
	BYTE cmd;


	if (disk_status(drv) & STA_NOINIT) return RES_NOTRDY;
	if (!(CardType & CT_BLOCK)) sector *= 512;	/* Convert LBA to byte address if needed */

	cmd = count > 1 ? CMD18 : CMD17;			/*  READ_MULTIPLE_BLOCK : READ_SINGLE_BLOCK */
	if (send_cmd(cmd, sector) == 0) {
		do {
			if (!rcvr_datablock(buff, 512)) break;
			buff += 512;
		} while (--count);
		if (cmd == CMD18) send_cmd(CMD12, 0);	/* STOP_TRANSMISSION */
	}
	deselect();

	return count ? RES_ERROR : RES_OK;
}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

DRESULT disk_write (
	BYTE drv,			/* Physical drive nmuber (0) */
	const BYTE *buff,	/* Pointer to the data to be written */
	DWORD sector,		/* Start sector number (LBA) */
	UINT count			/* Sector count (1..128) */
)
{
	if (disk_status(drv) & STA_NOINIT) return RES_NOTRDY;
	if (!(CardType & CT_BLOCK)) sector *= 512;	/* Convert LBA to byte address if needed */

	if (count == 1) {	/* Single block write */
		if ((send_cmd(CMD24, sector) == 0)	/* WRITE_BLOCK */
			&& xmit_datablock(buff, 0xFE))
			count = 0;
	}
	else {				/* Multiple block write */
		if (CardType & CT_SDC) send_cmd(ACMD23, count);
		if (send_cmd(CMD25, sector) == 0) {	/* WRITE_MULTIPLE_BLOCK */
			do {
				if (!xmit_datablock(buff, 0xFC)) break;
				buff += 512;
			} while (--count);
			if (!xmit_datablock(0, 0xFD))	/* STOP_TRAN token */
				count = 1;
		}
	}
	deselect();

	return count ? RES_ERROR : RES_OK;
}


/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

DRESULT disk_ioctl (
	BYTE drv,		/* Physical drive nmuber (0) */
	BYTE ctrl,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
{
	DRESULT res;
	BYTE n, csd[16];
	DWORD cs;


	if (disk_status(drv) & STA_NOINIT) return RES_NOTRDY;	/* Check if card is in the socket */

	res = RES_ERROR;
	switch (ctrl) {
		case CTRL_SYNC :		/* Make sure that no pending write process */
			if (select()) res = RES_OK;
			break;

		case GET_SECTOR_COUNT :	/* Get number of sectors on the disk (DWORD) */
			if ((send_cmd(CMD9, 0) == 0) && rcvr_datablock(csd, 16)) {
				if ((csd[0] >> 6) == 1) {	/* SDC ver 2.00 */
					cs = csd[9] + ((WORD)csd[8] << 8) + ((DWORD)(csd[7] & 63) << 16) + 1;
					*(DWORD*)buff = cs << 10;
				} else {					/* SDC ver 1.XX or MMC */
					n = (csd[5] & 15) + ((csd[10] & 128) >> 7) + ((csd[9] & 3) << 1) + 2;
					cs = (csd[8] >> 6) + ((WORD)csd[7] << 2) + ((WORD)(csd[6] & 3) << 10) + 1;
					*(DWORD*)buff = cs << (n - 9);
				}
				res = RES_OK;
			}
			break;

		case GET_BLOCK_SIZE :	/* Get erase block size in unit of sector (DWORD) */
			*(DWORD*)buff = 128;
			res = RES_OK;
			break;

		default:
			res = RES_PARERR;
	}

	deselect();

	return res;
}

//static DWORD get_fattime() {
//	how to get?
//}

//--------------------------------------------------------------------------------------

FATFS fs;		/* FatFs work area needed for each volume */


static int fatfs_mount(lua_State *L)
{
    //int spiId = luaL_checkinteger(L, 1);    
    //int result = platform_spi_close(spiId);
	if (FATFS_DEBUG)
		printf("fatfs_init>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\r\n");

	//DWORD fre_clust, fre_sect, tot_sect;
	// 挂载点
	u8 *mount_point = luaL_optstring(L, 1, "");
	FATFS_SPI_ID = luaL_optint(L, 2, 0); // SPI_1
	FATFS_SPI_CS = luaL_optint(L, 3, 3); // GPIO_3

	DRESULT re = f_mount(&fs, mount_point, 0);
	
	lua_pushinteger(L, re);
	if (re == FR_OK) {
		if (FATFS_DEBUG)
			printf("[FatFS]fatfs_init success\r\n");
	}
	else {
		if (FATFS_DEBUG)
			printf("[FatFS]fatfs_init FAIL!! re=%d\r\n", re);
	}

	if (FATFS_DEBUG)
		printf("fatfs_init<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\r\n");
    return 1;
}

static int fatfs_unmount(lua_State *L) {
	u8 *mount_point = luaL_optstring(L, 1, "");
	FRESULT re = f_unmount(mount_point);
	lua_pushinteger(L, re);
	return 1;
}

static int fatfs_mkfs(lua_State *L) {
	u8 *mount_point = luaL_optstring(L, 1, "");
	u8 sfd = luaL_optint(L, 2, 0);
	u8 au = luaL_optint(L, 3, 0);
	BYTE work[FF_MAX_SS];
	FRESULT re = f_mkfs(mount_point, sfd, au, work, sizeof work);
	lua_pushinteger(L, re);
	return 1;
}

static int fatfs_getfree(lua_State *L)
{
	DWORD fre_clust, fre_sect, tot_sect;
	// 挂载点
	u8 *mount_point = luaL_optstring(L, 1, "");
	FATFS *fs2;
	DRESULT re2 = f_getfree(mount_point, &fre_clust, &fs2);
	if (re2) {
		lua_pushnil(L);
		lua_pushinteger(L, re2);
		return 2;
	}
	/* Get total sectors and free sectors */
    tot_sect = (fs2->n_fatent - 2) * fs2->csize;
    fre_sect = fre_clust * fs2->csize;
	lua_newtable(L);

	lua_pushstring(L, "total_sectors");
	lua_pushinteger(L, tot_sect);
	lua_settable(L, -3);

	lua_pushstring(L, "free_sectors");
	lua_pushinteger(L, fre_sect);
	lua_settable(L, -3);

	lua_pushstring(L, "total_kb");
	lua_pushinteger(L, tot_sect / 2);
	lua_settable(L, -3);

	lua_pushstring(L, "free_kb");
	lua_pushinteger(L, fre_sect / 2);
	lua_settable(L, -3);
	
	return lua_gettop(L);
}

// ------------------------------------------------
// ------------------------------------------------

static int fatfs_mkdir(lua_State *L) {
	int luaType = lua_type( L, 1);
	if(luaType != LUA_TSTRING ) {
		lua_pushinteger(L, -1);
		lua_pushstring(L, "file path must string");
		return 2;
	}
	FRESULT re = f_mkdir(lua_tostring(L, 1));
	lua_pushinteger(L, re);
	return 1;
}

static int fatfs_lsdir(lua_State *L)
{
	//FIL Fil;			/* File object needed for each open file */
	DIR dir;
	FILINFO fileinfo;
	int luaType = lua_type( L, 1);
	if(luaType != LUA_TSTRING ) {
		lua_pushinteger(L, -1);
		lua_pushstring(L, "dir must string");
		return 2;
	}
	u8 *buf;
	int len;
	buf = lua_tolstring( L, 1, &len );
	u8 dirname[len+1];
	memcpy(dirname, buf, len);
	dirname[len] = 0x00;
	DRESULT re = f_opendir(&dir, dirname);
	if (re != FR_OK) {
		lua_pushinteger(L, re);
		return 1;
	}

	lua_pushinteger(L, 0);
	lua_newtable(L);
	while(f_readdir(&dir, &fileinfo) == FR_OK) {
		if(!fileinfo.fname[0]) break;

		lua_pushlstring(L, fileinfo.fname, strlen(fileinfo.fname));
		lua_newtable(L);
		
		lua_pushstring(L, "size");
		lua_pushinteger(L, fileinfo.fsize);
		lua_settable(L, -3);
		
		lua_pushstring(L, "date");
		lua_pushinteger(L, fileinfo.fdate);
		lua_settable(L, -3);
		
		lua_pushstring(L, "time");
		lua_pushinteger(L, fileinfo.ftime);
		lua_settable(L, -3);
		
		lua_pushstring(L, "attrib");
		lua_pushinteger(L, fileinfo.fattrib);
		lua_settable(L, -3);

		lua_pushstring(L, "isdir");
		lua_pushinteger(L, fileinfo.fattrib & AM_DIR);
		lua_settable(L, -3);

		lua_settable(L, -3);
	}
	f_closedir(&dir);
	printf("[FatFS] lua_gettop=%d\r\n", lua_gettop(L));
    return 2;
}

//-------------------------------------------------------------

static int fatfs_stat(lua_State *L) {
	int luaType = lua_type(L, 1);
	if(luaType != LUA_TSTRING ) {
		lua_pushinteger(L, -1);
		lua_pushstring(L, "file path must string");
		return 2;
	}
	FILINFO fileinfo;
	u8 *path = lua_tostring(L, 1);
	FRESULT re = f_stat(path, &fileinfo);
	lua_pushinteger(L, re);
	if (re == FR_OK) {
		lua_newtable(L);
		
		lua_pushstring(L, "size");
		lua_pushinteger(L, fileinfo.fsize);
		lua_rawset(L, -3);
		
		lua_pushstring(L, "date");
		lua_pushinteger(L, fileinfo.fdate);
		lua_rawset(L, -3);
		
		lua_pushstring(L, "time");
		lua_pushinteger(L, fileinfo.ftime);
		lua_rawset(L, -3);
		
		lua_pushstring(L, "attrib");
		lua_pushinteger(L, fileinfo.fattrib);
		lua_rawset(L, -3);

		lua_pushstring(L, "isdir");
		lua_pushinteger(L, fileinfo.fattrib & AM_DIR);
		lua_rawset(L, -3);
	}
	else {
		lua_pushnil(L);
	}
	return 2;
}

/**
 * fatfs.open("adc.txt") 
 * fatfs.open("adc.txt", 2) 
 */
static int fatfs_open(lua_State *L) {
	int luaType = lua_type( L, 1);
	if(luaType != LUA_TSTRING ) {
		lua_pushnil(L);
		lua_pushinteger(L, -1);
		lua_pushstring(L, "file path must string");
		return 3;
	}
	u8 *path  = lua_tostring(L, 1);

	FIL* fil = (FIL*)lua_newuserdata(L, sizeof(FIL));
	int flag = luaL_optinteger(L, 2, 1); // 第二个参数
	flag |= luaL_optinteger(L, 3, 0); // 第三个参数
	flag |= luaL_optinteger(L, 4, 0); // 第四个参数

	if (FATFS_DEBUG)
		printf("[FatFS]open %s %0X\r\n", path, flag);
	DRESULT re = f_open(fil, path, (BYTE)flag);
	if (re != FR_OK) {
		lua_remove(L, -1);
		lua_pushnil(L);
		lua_pushinteger(L, re);
		return 2;
	}
	return 1;
}

static int fatfs_close(lua_State *L) {
	int luaType = lua_type(L, 1);
	if(luaType != LUA_TUSERDATA ) {
		lua_pushinteger(L, -1);
		lua_pushstring(L, "must be FIL*");
		return 2;
	}
	FIL* fil = (FIL*)lua_touserdata(L, 1);
	FRESULT re = f_close(fil);
	//free(fil);
	lua_pushinteger(L, re);
	return 1;
}

static int fatfs_seek(lua_State *L) {
	int luaType = lua_type( L, 1);
	if(luaType != LUA_TUSERDATA ) {
		lua_pushinteger(L, -1);
		lua_pushstring(L, "must be FIL*");
		return 2;
	}
	UINT seek = luaL_optinteger(L, 2, 0);
	FRESULT re = f_lseek((FIL*)lua_touserdata(L, 1), seek);
	lua_pushinteger(L, re);
	return 1;
}

static int fatfs_truncate(lua_State *L) {
	int luaType = lua_type( L, 1);
	if(luaType != LUA_TUSERDATA ) {
		lua_pushinteger(L, -1);
		lua_pushstring(L, "must be FIL*");
		return 2;
	}
	FRESULT re = f_truncate((FIL*)lua_touserdata(L, 1));
	lua_pushinteger(L, re);
	return 1;
}

static int fatfs_read(lua_State *L) {
	int luaType = lua_type( L, 1);
	if(luaType != LUA_TUSERDATA ) {
		lua_pushinteger(L, -1);
		lua_pushstring(L, "must be FIL*");
		return 2;
	}
	UINT limit = luaL_optinteger(L, 2, 512);
	u8 buf[limit];
	UINT len;
	if (FATFS_DEBUG)
		printf("[FatFS]readfile limit=%d\r\n", limit);
	FRESULT re = f_read((FIL*)lua_touserdata(L, 1), buf, limit, &len);
	lua_pushinteger(L, re);
	if (re != FR_OK) {
		return 1;
	}
	lua_pushlstring(L, buf, len);
	return 2;
}

static int fatfs_write(lua_State *L) {
	int luaType = lua_type( L, 1);
	if(luaType != LUA_TUSERDATA ) {
		lua_pushinteger(L, -1);
		lua_pushstring(L, "must be FIL*");
		return 2;
	}
	FIL* fil = (FIL*)lua_touserdata(L, 1);
    luaType = lua_type( L, 2 );
    int len;
    u8* buf;
	int re = -1;
    
    if(luaType == LUA_TSTRING )
    {
        buf = lua_tolstring( L, 2, &len );
        
        re = f_write(fil, buf, len, &len);
    }
    else if(luaType == LUA_TLIGHTUSERDATA)
    {         
         buf = lua_touserdata(L, 2);
         len = lua_tointeger( L, 3);
         
         re = f_write(fil, buf, len, &len);
    }
    if (FATFS_DEBUG)
		printf("[FatFS]write re=%d len=%d\r\n", re, len);
    lua_pushinteger(L, re);
    lua_pushinteger(L, len);
    return 2;
}

static int fatfs_remove(lua_State *L) {
	int luaType = lua_type(L, 1);
	if(luaType != LUA_TSTRING ) {
		lua_pushinteger(L, -1);
		lua_pushstring(L, "file path must string");
		return 2;
	}
	FRESULT re = f_unlink(lua_tostring(L, 1));
	lua_pushinteger(L, re);
	return 1;
}

static int fatfs_rename(lua_State *L) {
	int luaType = lua_type(L, 1);
	if(luaType != LUA_TSTRING ) {
		lua_pushinteger(L, -1);
		lua_pushstring(L, "source file path must string");
		return 2;
	}
	luaType = lua_type(L, 2);
	if(luaType != LUA_TSTRING ) {
		lua_pushinteger(L, -1);
		lua_pushstring(L, "dest file path must string");
		return 2;
	}
	FRESULT re = f_rename(lua_tostring(L, 1), lua_tostring(L, 2));
	lua_pushinteger(L, re);
	return 1;
}



/**
 * fatfs.readfile("adc.txt") 
 * fatfs.readfile("adc.txt", 512, 0) 默认只读取512字节,从0字节开始读
 */
static int fatfs_readfile(lua_State *L) {
	int luaType = lua_type( L, 1);
	if(luaType != LUA_TSTRING ) {
		lua_pushinteger(L, -1);
		lua_pushstring(L, "file path must string");
		return 2;
	}
	FIL fil;

	DRESULT re = f_open(&fil, lua_tostring(L, 1), FA_READ);
	if (re != FR_OK) {
		lua_pushinteger(L, re);
		return 1;
	}

	u32 limit = luaL_optinteger(L, 2, 512);
	u32 seek = luaL_optinteger(L, 3, 0);
	if (seek > 0) {
		f_lseek(&fil, seek);
	}

	u8 buf[limit];
	UINT len;
	if (FATFS_DEBUG)
		printf("[FatFS]readfile seek=%d limit=%d\r\n", seek, limit);
	FRESULT fr = f_read(&fil, buf, limit, &len);
	if (fr != FR_OK) {
		lua_pushinteger(L, -3);
		lua_pushinteger(L, fr);
		return 2;
	}
	f_close(&fil);
	lua_pushinteger(L, 0);
	lua_pushlstring(L, buf, len);
	if (FATFS_DEBUG)
		printf("[FatFS]readfile seek=%d limit=%d len=%d\r\n", seek, limit, len);
	return 2;
}

static int fatfs_playmp3(lua_State *L) {
	int luaType = lua_type( L, 1);
	if(luaType != LUA_TSTRING ) {
		lua_pushinteger(L, -1);
		lua_pushstring(L, "file path must string");
		return 2;
	}
	FILINFO fileinfo;
	u8 *path = lua_tostring(L, 1);
	FRESULT re = f_stat(path, &fileinfo);
	if (re) {
		lua_pushinteger(L, re);
		return 1;
	}
	FIL fil;
	re = f_open(&fil, path, FA_READ);
	if (re != FR_OK) {
		lua_pushinteger(L, re);
		return 1;
	}

	u8 buf[fileinfo.fsize];
	UINT len;
	FRESULT fr = f_read(&fil, buf, fileinfo.fsize, &len);
	if (fr) {
		lua_pushinteger(L, fr);
		return 1;
	}
	f_close(&fil);
	AudioPlayParam param;
    
    param.isBuffer = TRUE;
    param.buffer.format = PLATFORM_AUD_MP3;
    param.buffer.loop = 0;
    param.buffer.data = buf;
    param.buffer.len = len;

	re = platform_audio_play(&param);
	lua_pushinteger(L, re);
	return 1;
}

static int fatfs_debug_mode(lua_State *L) {
	FATFS_DEBUG = luaL_optinteger(L, 1, 1);
	return 0;
}

// Module function map
const LUA_REG_TYPE fatfs_map[] =
{ 
  { LSTRKEY( "init" ),  LFUNCVAL(fatfs_mount ) }, //初始化,挂载, 别名方法
  { LSTRKEY( "mount" ),  LFUNCVAL(fatfs_mount ) }, //初始化,挂载
  { LSTRKEY( "unmount" ),  LFUNCVAL(fatfs_unmount ) }, // 取消挂载
  { LSTRKEY( "mkfs" ),  LFUNCVAL(fatfs_mkfs ) }, // 格式化!!!
  //{ LSTRKEY( "test" ),  LFUNCVAL( fatfs_test ) },
  { LSTRKEY("getfree"), LFUNCVAL(fatfs_getfree)}, // 获取文件系统大小,剩余空间
  { LSTRKEY( "debug" ),  LFUNCVAL( fatfs_debug_mode ) }, // 调试模式,打印更多日志

  { LSTRKEY( "lsdir" ),  LFUNCVAL( fatfs_lsdir ) }, // 列举目录下的文件,名称,大小,日期,属性
  { LSTRKEY( "mkdir" ),  LFUNCVAL( fatfs_mkdir ) }, // 列举目录下的文件,名称,大小,日期,属性

  { LSTRKEY( "stat" ),  LFUNCVAL( fatfs_stat ) }, // 查询文件信息
  { LSTRKEY( "open" ),  LFUNCVAL( fatfs_open ) }, // 打开一个文件句柄
  { LSTRKEY( "close" ),  LFUNCVAL( fatfs_close ) }, // 关闭一个文件句柄
  { LSTRKEY( "seek" ),  LFUNCVAL( fatfs_seek ) }, // 移动句柄的当前位置
  { LSTRKEY( "truncate" ),  LFUNCVAL( fatfs_truncate ) }, // 缩减文件尺寸到当前seek位置
  { LSTRKEY( "read" ),  LFUNCVAL( fatfs_read ) }, // 读取数据
  { LSTRKEY( "write" ),  LFUNCVAL( fatfs_write ) }, // 写入数据
  { LSTRKEY( "remove" ),  LFUNCVAL( fatfs_remove ) }, // 删除文件,别名方法
  { LSTRKEY( "unlink" ),  LFUNCVAL( fatfs_remove ) }, // 删除文件
  { LSTRKEY( "rename" ),  LFUNCVAL( fatfs_rename ) }, // 文件改名

  { LSTRKEY( "readfile" ),  LFUNCVAL( fatfs_readfile ) }, // 读取文件的简易方法
  { LSTRKEY( "playmp3" ),  LFUNCVAL( fatfs_playmp3 ) }, // 读取文件的简易方法

  { LNILKEY, LNILVAL }
};

LUALIB_API int luaopen_fatfs( lua_State *L )
{
  luaL_register( L, AUXLIB_FATFS, fatfs_map );
  // mark for open
  MOD_REG_NUMBER(L, "FA_READ", FA_READ);
  MOD_REG_NUMBER(L, "FA_WRITE", FA_WRITE);
  MOD_REG_NUMBER(L, "FA_CREATE_ALWAYS", FA_CREATE_ALWAYS);
  MOD_REG_NUMBER(L, "FA_CREATE_NEW", FA_CREATE_NEW);
  MOD_REG_NUMBER(L, "FA_OPEN_ALWAYS", FA_OPEN_ALWAYS);
  MOD_REG_NUMBER(L, "FA_OPEN_APPEND", FA_OPEN_APPEND);
  MOD_REG_NUMBER(L, "FA_OPEN_EXISTING", FA_OPEN_EXISTING);
  // mark for result
  MOD_REG_NUMBER(L, "FR_OK",FR_OK);				/* (0) Succeeded */
  MOD_REG_NUMBER(L, "FR_DISK_ERR",FR_DISK_ERR);			/* (1) A hard error occurred in the low level disk I/O layer */
  MOD_REG_NUMBER(L, "FR_INT_ERR",FR_INT_ERR);				/* (2) Assertion failed */
  MOD_REG_NUMBER(L, "FR_NOT_READY",FR_NOT_READY);			/* (3) The physical drive cannot work */
  MOD_REG_NUMBER(L, "FR_NO_FILE",FR_NO_FILE);				/* (4) Could not find the file */
  MOD_REG_NUMBER(L, "FR_NO_PATH",FR_NO_PATH);				/* (5) Could not find the path */
  MOD_REG_NUMBER(L, "FR_INVALID_NAME",FR_INVALID_NAME);		/* (6) The path name format is invalid */
  MOD_REG_NUMBER(L, "FR_DENIED",FR_DENIED);				/* (7) Access denied due to prohibited access or directory full */
  MOD_REG_NUMBER(L, "FR_EXIST",FR_EXIST);				/* (8) Access denied due to prohibited access */
  MOD_REG_NUMBER(L, "FR_INVALID_OBJECT",FR_INVALID_OBJECT);		/* (9) The file/directory object is invalid */
  MOD_REG_NUMBER(L, "FR_WRITE_PROTECTED",FR_WRITE_PROTECTED);		/* (10) The physical drive is write protected */
  MOD_REG_NUMBER(L, "FR_INVALID_DRIVE",FR_INVALID_DRIVE);		/* (11) The logical drive number is invalid */
  MOD_REG_NUMBER(L, "FR_NOT_ENABLED",FR_NOT_ENABLED);			/* (12) The volume has no work area */
  MOD_REG_NUMBER(L, "FR_NO_FILESYSTEM",FR_NO_FILESYSTEM);		/* (13) There is no valid FAT volume */
  MOD_REG_NUMBER(L, "FR_MKFS_ABORTED",FR_MKFS_ABORTED);		/* (14) The f_mkfs() aborted due to any problem */
  MOD_REG_NUMBER(L, "FR_TIMEOUT",FR_TIMEOUT);				/* (15) Could not get a grant to access the volume within defined period */
  MOD_REG_NUMBER(L, "FR_LOCKED",FR_LOCKED);				/* (16) The operation is rejected according to the file sharing policy */
  MOD_REG_NUMBER(L, "FR_NOT_ENOUGH_CORE",FR_NOT_ENOUGH_CORE);		/* (17) LFN working buffer could not be allocated */
  MOD_REG_NUMBER(L, "FR_TOO_MANY_OPEN_FILES",FR_TOO_MANY_OPEN_FILES);	/* (18) Number of open files > FF_FS_LOCK */
  MOD_REG_NUMBER(L, "FR_INVALID_PARAMETER",FR_INVALID_PARAMETER);	/* (19) Given parameter is invalid */
  return 1;
}
