#ifdef LUA_QXWZ_LIB
#include "config.h"
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "platform.h"
#include "auxmods.h"
#include "lrotable.h"
#include "platform_conf.h"
#include "platform_pmd.h"
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include "platform.h"
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "qxwz_types.h"
#include "qxwz_sdk.h"
#include "qxwz_socket.h"
#include "qxwz_status.h"
#include "debug.h"

#ifndef __INT8_T__
typedef char        int8_t;
#define __INT8_T__
#endif

#ifndef __INT16_T__
typedef short       int16_t;
#define __INT16_T__
#endif

#ifndef __INT32_T__
typedef int         int32_t;
#define __INT32_T__
#endif

#ifndef __U8_T__
typedef unsigned char u8_t;
#define __U8_T__
#endif

#ifndef __UINT8_T__
typedef unsigned char   uint8_t;
#define __UINT8_T__
#endif

#ifndef __UINT16_T__
typedef unsigned short  uint16_t;
#define __UINT16_T__
#endif

#ifndef __U16_T__
typedef unsigned short u16_t;
#define __U16_T__
#endif


#ifndef __UINT32_T__
typedef unsigned int    uint32_t;
#define __UINT32_T__
#endif

typedef unsigned long long  uint64_t;
//带长度控制的缓冲结构
typedef struct
{
	uint8_t *Data;		//数据指针
	uint32_t Pos;		//包含的字节数
	uint32_t MaxLen;	//该缓冲区容量
}Buffer_Struct;

typedef struct
{
	soc_cb_t cb;
    qxwz_s8_t *keyInfo[4];
    qxwz_usr_config_t config;
    qxwz_data_response_t data_rsp;
    qxwz_status_response_t status_rsp;
    Buffer_Struct TxBuf;
    Buffer_Struct RxBuf;
    Buffer_Struct DataBuf;
    qxwz_s8_t *hostName;
    qxwz_u16_t port;
    qxwz_s32_t status;
    qxwz_s32_t socket;
    qxwz_u8_t isOnline;
    qxwz_u8_t isTxBusy;
    qxwz_u8_t NeedMoreSend;
    qxwz_u32_t tick;
}QXWZ_CtrlStruct;

enum
{
	QXWZ_CONNECT,
	QXWZ_SEND,
	QXWZ_RECV,
	QXWZ_CLOSE,
	QXWZ_RTCM,
};

static QXWZ_CtrlStruct gQXWZ;
extern int32_t qxwz_printf(const char *fmt, ...);
extern int32_t rand();
int32_t qxwz_printf(const char *fmt, ...)
{
//    va_list ap;
//    int ret;
//
//    va_start(ap, fmt);
//    ret = lualibc_vfprintf(stdout, fmt, ap);
//    va_end(ap);
//
//    return ret;
	return -1;
}
int32_t rand()
{
	return lua_rand();
}
int32_t OS_InitBuffer(Buffer_Struct *Buf, uint32_t Size)
{
	if (!Buf)
		return 0;
	Buf->Data = malloc(Size);
	if (!Buf->Data)
	{
		Buf->MaxLen = 0;
		Buf->Pos = 0;
		return NULL;
	}
	Buf->MaxLen = Size;
	Buf->Pos = 0;
	return Size;
}

void OS_DeInitBuffer(Buffer_Struct *Buf)
{
	if (Buf->Data)
	{
		free(Buf->Data);
	}
	Buf->Data = NULL;
	Buf->MaxLen = 0;
	Buf->Pos = 0;
}

int32_t OS_ReInitBuffer(Buffer_Struct *Buf, uint32_t Size)
{
	if (!Buf)
		return 0;

	if (Buf->Data)
	{
		free(Buf->Data);
	}
	Buf->Data = malloc(Size);
	if (!Buf->Data)
	{
		Buf->MaxLen = 0;
		Buf->Pos = 0;
		return 0;
	}
	Buf->MaxLen = Size;
	Buf->Pos = 0;
	return Size;
}

int32_t OS_ReSizeBuffer(Buffer_Struct *Buf, uint32_t Size)
{
	uint8_t *Old;
	uint8_t *New;

	if (!Buf)
		return 0;

	Old = Buf->Data;
	if (Size < Buf->Pos)
	{
		Size = Buf->Pos;
	}
	New = malloc(Size);
	if (!New)
	{
		return 0;
	}
	if (Old)
	{
		memcpy(New, Old, Buf->Pos);
		free(Old);
	}

	Buf->Data = New;
	Buf->MaxLen = Size;
	return Size;
}

int32_t OS_BufferWrite(Buffer_Struct *Buf, void *Data, uint32_t Len)
{
	uint32_t WriteLen;
	if (!Len)
	{
		return 0;
	}
	if (!Buf)
	{
		return -1;
	}
	if (!Buf->Data)
	{
		Buf->Data = malloc(Len);
		if (!Buf->Data)
		{
			return -1;
		}
		Buf->Pos = 0;
		Buf->MaxLen = Len;
	}
	WriteLen = Buf->Pos + Len;
	if (WriteLen > Buf->MaxLen)
	{
		if (!OS_ReSizeBuffer(Buf, WriteLen))
		{
			return -1;
		}
	}
	memcpy(&Buf->Data[Buf->Pos], Data, Len);
	Buf->Pos += Len;
	return 0;
}

int32_t OS_BufferWriteLimit(Buffer_Struct *Buf, void *Data, uint32_t Len)
{
	uint32_t WriteLen;
	if (!Len)
	{
		return 0;
	}
	if (!Buf)
	{
		return -1;
	}
	if (!Buf->Data)
	{
		Buf->Data = malloc(Len);
		if (!Buf->Data)
		{
			return -1;
		}
		Buf->Pos = 0;
		Buf->MaxLen = Len;
	}
	WriteLen = Buf->Pos + Len;
	if (WriteLen > Buf->MaxLen)
	{
		return -1;
	}
	memcpy(&Buf->Data[Buf->Pos], Data, Len);
	Buf->Pos += Len;
	return 0;
}

void OS_BufferRemove(Buffer_Struct *Buf, uint32_t Len)
{
	uint32_t RestLen;
	uint32_t i;
	if (!Buf)
		return ;
	if (!Buf->Data)
		return ;
	if (Len >= Buf->Pos)
	{
		Buf->Pos = 0;
		return ;
	}
	RestLen = Buf->Pos - Len;
	for(i = 0; i < RestLen; i++)
	{
		Buf->Data[i] = Buf->Data[Len + i];
	}
	Buf->Pos = RestLen;
}

void QXWZ_SendMsg(uint8_t Type)
{
    PlatformMessage *rtosmsg;
    rtosmsg = platform_calloc(1, sizeof(PlatformMessage));
    rtosmsg->id = RTOS_MSG_QXWZ;
    rtosmsg->data.QXWZData.Type = Type;
    platform_rtos_send(rtosmsg);
}

/**
 * @param[in]  nwk_id: network identifier.
 *                     0, By default, then depends on user to choose.
 *                     1, China telecom cellular system
 *                     2, China mobile cellular system
 *                     3, China unicom cellular system
 *                     4, Wi-Fi
 *                     5, Wired
 *                     6, Others
 *
 * @return:
 *    -1 if fail
 *    socket number if okay
 */
qxwz_s32_t qxwz_soc_create(qxwz_u32_t nwk_id)
{
	if (gQXWZ.socket)
	{
		//DBG("!");
		return -1;
	}
	if (gQXWZ.isOnline)
	{
		//DBG("!");
		return -1;
	}
	gQXWZ.socket = 2;
	return gQXWZ.socket;
}


/**
 * @param[in]  soc: socket number.
 * @param[in]  host: the server info @see host_info_t.
 * @param[in]  cbs: the callbacks for async @see soc_cb_t.
 *
 * @return:
 *   0 if success
 *  -1 if fail
 *  -2 if async occurs
 */
qxwz_s32_t qxwz_soc_connect(qxwz_s32_t soc, host_info_t *host, soc_cb_t *cbs)
{
	if (gQXWZ.socket != soc)
	{
		//DBG("!");
		return -1;
	}
	if (gQXWZ.isOnline)
	{
		//DBG("!");
		return -1;
	}
	if (gQXWZ.hostName)
	{
		free(gQXWZ.hostName);
		gQXWZ.hostName = NULL;
	}
	gQXWZ.hostName = malloc(strlen(host->hostName) + 1);
	if (!gQXWZ.hostName)
	{
		return -1;
	}
	strcpy(gQXWZ.hostName, host->hostName);
	gQXWZ.port = host->port;
	gQXWZ.cb = *cbs;
	QXWZ_SendMsg(QXWZ_CONNECT);
	return -2;
}


/**
 * @param[in]  soc: socket number.
 * @param[in]  buf: the data buffer which will be sent.
 * @param[in]  len: the data buffer length.
 *
 * @return:
 *  >=0 if success
 *   -1 if fail for any reason
 *   -2 if buffer not available, async occurs
 */
qxwz_s32_t qxwz_soc_send(qxwz_s32_t soc, const qxwz_s8_t *buf, qxwz_s32_t len)
{
	if (gQXWZ.socket != soc)
	{
		//DBG("!");
		return -1;
	}
	if (!gQXWZ.isOnline)
	{
		//DBG("!");
		return -1;
	}
	if (gQXWZ.isTxBusy)
	{
		gQXWZ.NeedMoreSend = 1;
		return -2;
	}
	OS_BufferWrite(&gQXWZ.TxBuf, buf, len);
	QXWZ_SendMsg(QXWZ_SEND);
	gQXWZ.isTxBusy = 1;
	return len;
}

/**
 * @param[in]  soc: socket number.
 * @param[out] buf: the data buffer which will be filled by received data.
 * @param[in]  len: the maxium lenght of data buffer.
 *
 * @return:
 * >=0 if success
 *  -1 if fail for any reason, or receive the FIN from the server
 *  -2 if no data available, async occurs
 */
qxwz_s32_t qxwz_soc_recv(qxwz_s32_t soc, qxwz_s8_t *buf, qxwz_s32_t len)
{
	qxwz_s32_t dummylen;
	if (gQXWZ.socket != soc)
	{
		//DBG("!");
		return -1;
	}
	if (!gQXWZ.isOnline)
	{
		//DBG("!");
		return -1;
	}
	if (gQXWZ.RxBuf.Pos)
	{
		dummylen = (gQXWZ.RxBuf.Pos > len)?len:gQXWZ.RxBuf.Pos;
		memcpy(buf, gQXWZ.RxBuf.Data, dummylen);
		OS_BufferRemove(&gQXWZ.RxBuf, dummylen);
		return dummylen;
	}
	return -2;
}

/**
 * @param[in]  soc: socket number.
 *
 * @return:
 *   0 if success
 *  -1 if fail
 *  -2 if async occurs
 */
qxwz_s32_t qxwz_soc_close(qxwz_s32_t soc)
{
	if (gQXWZ.isOnline)
	{
		QXWZ_SendMsg(QXWZ_CLOSE);
	}
	gQXWZ.socket = 0;
	gQXWZ.isOnline = 0;
	gQXWZ.isTxBusy = 0;
	gQXWZ.NeedMoreSend = 0;
	memset(&gQXWZ.cb, 0, sizeof(gQXWZ.cb));
	return 0;
}

static int QXWZ_setting( lua_State *L )
{
	const char *s[4];
	u32 slen[4];
	u8 arg_index = 1, i;
	int result = 0;

	for(i = 0; i < 4; i++)
	{
		if (gQXWZ.keyInfo[i])
		{
			free(gQXWZ.keyInfo[i]);
			gQXWZ.keyInfo[i] = NULL;
		}
	}

	for (arg_index = 1; arg_index <= 4; arg_index++)
	{
		s[arg_index - 1] = luaL_checklstring(L, arg_index, &slen[arg_index - 1]);
		//DBG("config %d %d %s", arg_index, slen[arg_index - 1], s[arg_index - 1]);
		if (!s[arg_index - 1])
		{
			result = -1;
			break;
		}
	}
	if (!result)
	{
		for(i = 0; i < 4; i++)
		{
			gQXWZ.keyInfo[i] = malloc(slen[i] + 1);
			if (!gQXWZ.keyInfo[i])
			{
				result = -1;
				break;
			}
			else
			{
				memset(gQXWZ.keyInfo[i], 0, slen[i] + 1);
				memcpy(gQXWZ.keyInfo[i], s[i], slen[i]);
			}
		}
		if (!result)
		{
			gQXWZ.config.appkey = gQXWZ.keyInfo[0];
			gQXWZ.config.appsecret = gQXWZ.keyInfo[1];
			gQXWZ.config.device_ID = gQXWZ.keyInfo[2];
			gQXWZ.config.device_Type = gQXWZ.keyInfo[3];
			result = qxwz_setting(&gQXWZ.config, TRUE);
			//DBG("result %d", result);
		}
	}
	lua_pushinteger( L, result );
    return 1;
}

qxwz_void_t qxwz_cb_ipdata(qxwz_void_t *data, qxwz_u32_t length, qxwz_data_type_e type)
{
	//DBG("got ip rtcm %d", type);
	OS_BufferWrite(&gQXWZ.DataBuf, data, length);
	QXWZ_SendMsg(QXWZ_RTCM);

}
qxwz_void_t qxwz_cb_stdata(qxwz_void_t *data, qxwz_u32_t length, qxwz_data_type_e type)
{

}
qxwz_void_t qxwz_cb_status(qxwz_s32_t status)
{
	//DBG("got rtcm status=%d",status);
	gQXWZ.status = status;
}

static int QXWZ_start( lua_State *L )
{
	int result = 0;
	result = qxwz_start(&gQXWZ.data_rsp, &gQXWZ.status_rsp);
	gQXWZ.tick = 0;
	lua_pushinteger( L, result );
    return 1;
}

static int QXWZ_tick( lua_State *L )
{
	int result = 0;
	uint32_t tick = platform_get_tamp() - 8 * 3600;
	//DBG("%u", tick);
	result = qxwz_tick(tick);
//	gQXWZ.tick++;
	lua_pushinteger( L, result );
    return 1;
}

static int QXWZ_release( lua_State *L )
{
	int result = 0;
	qxwz_release();
	lua_pushinteger( L, result );
    return 1;
}

static int QXWZ_stop( lua_State *L )
{
	int result = 0;
	qxwz_stop();

	lua_pushinteger( L, result );
    return 1;
}

static int QXWZ_send( lua_State *L )
{
	const char *s;
	u32 slen;
	int result = 0;

	s = luaL_checklstring(L, 1, &slen);
	if (!s)
	{
		result = -1;
	}
	else
	{
		qxwz_send_data(s,slen,UDATA_GGA);
	}
	lua_pushinteger( L, result );
    return 1;
}

static int QXWZ_gethost( lua_State *L )
{
	int result = 0;
	if (gQXWZ.hostName)
	{
		//DBG("%s,%d",gQXWZ.hostName,gQXWZ.port);
		lua_pushinteger( L, result );
		lua_pushlstring( L, gQXWZ.hostName,strlen(gQXWZ.hostName));
		lua_pushinteger( L, gQXWZ.port );
		return 3;
	}
	else
	{
		result = -1;
		lua_pushinteger( L, result );
		return 1;
	}


}

static int QXWZ_getrtcm( lua_State *L )
{
	int result = 0;

	if (gQXWZ.DataBuf.Pos)
	{
		lua_pushinteger( L, result );
		lua_pushlstring( L, gQXWZ.DataBuf.Data,  gQXWZ.DataBuf.Pos);
		gQXWZ.DataBuf.Pos = 0;
		return 2;
	}
	else
	{
		result = -1;
		lua_pushinteger( L, result );
	}

    return 1;
}

static int QXWZ_getstatus( lua_State *L )
{
	lua_pushinteger( L, gQXWZ.status );
    return 1;
}

static int QXWZ_netconnectok( lua_State *L )
{
	gQXWZ.isTxBusy = 0;
	gQXWZ.isOnline = 1;
	gQXWZ.NeedMoreSend = 0;
	if (gQXWZ.cb.cb_connect && gQXWZ.socket)
	{
		gQXWZ.cb.cb_connect(gQXWZ.socket);
	}

	return 1;
}

static int QXWZ_getsend( lua_State *L )
{
	int result = 0;

	if (gQXWZ.TxBuf.Pos)
	{
		lua_pushinteger( L, result );
		lua_pushlstring( L, gQXWZ.TxBuf.Data,  gQXWZ.TxBuf.Pos);
		gQXWZ.TxBuf.Pos = 0;
		return 2;
	}
	else
	{
		result = -1;
		lua_pushinteger( L, result );
	}

    return 1;
}

static int QXWZ_netsendok( lua_State *L )
{
	gQXWZ.isTxBusy = 0;
	if (gQXWZ.NeedMoreSend)
	{
		gQXWZ.NeedMoreSend = 0;
		if (gQXWZ.cb.cb_send && gQXWZ.socket)
		{
			gQXWZ.cb.cb_send(gQXWZ.socket);
		}
	}

	lua_pushinteger( L, 0 );
	return 1;
}

static int QXWZ_netcloseok( lua_State *L )
{
	if (gQXWZ.cb.cb_close && gQXWZ.socket)
	{
		gQXWZ.cb.cb_close(gQXWZ.socket);
	}
	gQXWZ.socket = 0;
	gQXWZ.isOnline = 0;
	lua_pushinteger( L, 0 );
	return 1;
}

static int QXWZ_netrecv( lua_State *L )
{
	const char *s;
	u32 slen;
	int result = 0;

	s = luaL_checklstring(L, 1, &slen);
	if (!s)
	{
		result = -1;
	}
	else
	{
		//OS_BufferWrite(&gQXWZ.RxBuf, s, slen);
		if (gQXWZ.cb.cb_recv && gQXWZ.socket)
		{
			gQXWZ.cb.cb_recv(gQXWZ.socket, s, slen);
		}

	}
	lua_pushinteger( L, result );
    return 1;
}


#define MIN_OPT_LEVEL   2
#include "lrodefs.h"

const LUA_REG_TYPE qxwz_map[] =
{
	{ LSTRKEY( "setting" ),  LFUNCVAL( QXWZ_setting ) },
	{ LSTRKEY( "start" ),  LFUNCVAL( QXWZ_start ) },
	{ LSTRKEY( "tick" ),  LFUNCVAL( QXWZ_tick ) },
	{ LSTRKEY( "send" ),  LFUNCVAL( QXWZ_send ) },
	{ LSTRKEY( "release" ),  LFUNCVAL( QXWZ_release ) },
	{ LSTRKEY( "stop" ),  LFUNCVAL( QXWZ_stop ) },
	{ LSTRKEY( "getstatus" ),  LFUNCVAL( QXWZ_getstatus ) },
	{ LSTRKEY( "gethost" ),  LFUNCVAL( QXWZ_gethost ) },
	{ LSTRKEY( "getrtcm" ),  LFUNCVAL( QXWZ_getrtcm ) },
	{ LSTRKEY( "getsend" ),  LFUNCVAL( QXWZ_getsend ) },
	{ LSTRKEY( "netconnectok" ),  LFUNCVAL( QXWZ_netconnectok ) },
	{ LSTRKEY( "netsendok" ),  LFUNCVAL( QXWZ_netsendok ) },
	{ LSTRKEY( "netrecv" ),  LFUNCVAL( QXWZ_netrecv ) },
	{ LSTRKEY( "netcloseok" ),  LFUNCVAL( QXWZ_netcloseok ) },

	{ LNILKEY, LNILVAL }
};

int luaopen_qxwz( lua_State *L )
{
    luaL_register( L, AUXLIB_QXWZ, qxwz_map );
    memset(&gQXWZ, 0, sizeof(gQXWZ));
    gQXWZ.data_rsp.cb_ipdata = qxwz_cb_ipdata;
    gQXWZ.data_rsp.cb_stdata = qxwz_cb_stdata;
    gQXWZ.status_rsp.cb_status = qxwz_cb_status;
    lualibc_rand();
    return 1;
}

#endif
