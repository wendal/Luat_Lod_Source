module(...,package.seeall)

local tag = "SDCard"
local pin_cs = pio.P0_3
local spi_cs = pins.setup(pin_cs, 1)
local FF = string.fromHex("FF")
local FF512 = ""
local RESP_R1 = string.char(0x01)
local RESP_R1B = string.char(0x01)

local jj = 0
while jj < 512 do
    jj = jj + 1
    FF512 = FF512 .. FF
end

local function sdcard_send_cmd(id, _cmd, _args, _crc, resp_len) 
    log.info(tag,"CMD STA ------------------------------------------")
    spi.send(id, FF)
    spi_cs(0)
    local t = string.char(_cmd + 0x40) .. string.fromHex(_args .. _crc)
    log.info(tag, "Send CMD" .. tostring(_cmd), spi.send(id, t))
    local i = 1
    local re2 = nil
    while true do
        re = spi.recv(id, 1)
        local _, resp = pack.unpack(re, ">b")
        log.info("SdCard", string.len(re), resp, string.toHex(re))
        
        if re ~= FF then
            if _cmd == 41 or _cmd == 8 or _cmd == 58 then
                re2 = spi.send_recv(id, FF .. FF .. FF .. FF)
            end
            if _cmd == 9 then
                local j = 0
                re2 = ""
                while j < 20 do
                    re2 = spi.send_recv(id, FF)
                    if re2 == string.fromHex("FE") then
                        re2 = ""
                        break
                    end
                    j = j + 1
                end
                j = 0
                while j < 18 do
                    re2 = re2 .. spi.send_recv(id, FF)
                    j = j + 1
                end
            end
            if _cmd == 17 then
                if re == string.char(0x00) then
                    log.info("CMD17", "SDCard resp OK")
                    -- 继续读,直至读到0xFE
                    local j = 0
                    while j < 20 do
                        re2 = spi.send_recv(id, FF)
                        if re2 == string.fromHex("FE") then
                            log.info("CMD17","GET Data Start flag 0xFE")
                            break
                        end
                        j = j + 1
                    end
                    re2 = ""
                    j = 0
                    --while j < 512 do
                    --    re2 = re2 .. spi.send_recv(id, FF)
                    --    j = j + 1
                    --end
                    re2 = spi.send_recv(id, FF512)
                    local crc2 = spi.send_recv(id, FF .. FF)
                    log.info("CMD17","DATA", "read DONE")
                    --log.info("CMD17","DATA", string.toHex(re2))
                    --log.info("CMD17","CRC", string.toHex(crc2))
                end
            end
            log.info("SdCard", "GET Response!!!", string.toHex(re))
            break
        end
        i = i + 1
        if i > 200 then
            log.info("SdCard", "wait timeout")
            return 0
        end
    end
    if cmd ~= 55 then
        spi_cs(1)
    end
    spi.send(id, FF)
    log.info(tag,"CMD END ------------------------------------------")
    return re, re2
end

function init()
    --打开SPI引脚的供电
    pmd.ldoset(6,pmd.LDO_VMMC) 

    --local re = spi.sdcard_init(1, pin_cs)
    --log.warn(tag, tostring(re))

    sys.wait(10)

    local id = spi.SPI_1
    local isOK = spi.setup(id,0,0,8,400*1000,1)
    if isOK ~= 1 then
        log.warn(tag,"spi init fail")
        return
    end
    log.info(tag, "spi init ok")
    -- 拉低CS
    spi_cs(1)

    -- 发送大于74个时钟周期. 发80个够不够!!
    local i = 0
    while i < 10 do
        spi.send(id, FF)
        i = i + 1
    end
    -- 发CMD0
    sdcard_send_cmd(id, 0, "00000000", "95", "R1")
    -- 发CMD8
    sdcard_send_cmd(id, 8, "000001AA", "87", "R3")
    i = 0
    while i < 20 do
        -- 发送CMD55 + ACMD41
        sdcard_send_cmd(id, 55, "00000000", "FF", "R1")
        local re, re2 = sdcard_send_cmd(id, 41, "40000000,", "FF", "R3")
        if re == string.char(0x00) then
            log.info(tag,"init complete!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!", string.toHex(re2))
            break
        end
        i = i + 1
    end
    -- 发送 CMD58
    local re, re2 = sdcard_send_cmd(id, 58, "00000000,", "FF", "R3")
    log.info(tag,"CMD58 resp", string.toHex(re2))
    -- 发送 CMD9
    local re, re2 = sdcard_send_cmd(id, 9, "00000000,", "FF", "R3")
    log.info(tag,"CMD9 resp", string.toHex(re2))
    local sdc = {}
    local nextpos, c0, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15 = pack.unpack(re2, ">b16")
    if bit.isset(c0, 7) or bit.isset(c0, 6) then
        t7 = bit.clear(c8, 7, 6)
        log.info(tag,"CSD version ~= 0", c7, c8, c9, c10)
        local sz = c10 + c9 * 256 + c8 * 256 * 256 + 1
        log.info(tag, "count = ", tostring(sz))
        log.info(tag, "SDCard", tostring(sz / 2), "mb")
        log.info(tag, "SDCard", tostring(sz / 2 / 1024), "gb")
    else
        --log.info(tag,"CSD version == 0")
    end
    -- 读第0块, 单块读取 CMD17
    --local re, re2 = sdcard_send_cmd(id, 17, "00000000", "FF")
    --local re, re2 = sdcard_send_cmd(id, 17, "00000000", "FF")
    --local re, re2 = sdcard_send_cmd(id, 17, "00000001", "FF") -- 貌似非法
    --local re, re2 = sdcard_send_cmd(id, 17, "00000200", "FF")
    --local re, re2 = sdcard_send_cmd(id, 17, "00008192", "FF")

    -- 关闭spi, 然后用更高的频率打开
    --spi.close(id)
    --spi.setup(id,0,0,8,1000*1000,1)
end

function test2()
    spi.sdcard_test()
end