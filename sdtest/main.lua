--必须在这个位置定义PROJECT和VERSION变量
--PROJECT：ascii string类型，可以随便定义，只要不使用,就行
--VERSION：ascii string类型，如果使用Luat物联云平台固件升级的功能，必须按照"X.X.X"定义，X表示1位数字；否则可随便定义
PROJECT = "TestSDCard"
VERSION = "1.0.0"

RT={}

--加载日志功能模块，并且设置日志输出等级
--如果关闭调用log模块接口输出的日志，等级设置为log.LOG_SILENT即可
require "log"
LOG_LEVEL = log.LOGLEVEL_TRACE

require "sys"
require "utils"

require "net"
--每1分钟查询一次GSM信号强度
--每1分钟查询一次基站信息
net.startQueryAll(60000, 60000)

-- 时间核对
--require "ntp"
--ntp.timeSync(12)

-- 必须启用监控狗
require "wdt"
wdt.setup(pio.P0_30, pio.P0_31)

-- 总是加载S6模块上的网络灯
require "netLed"
netLed.setup(true)

require "misc"
require "audio"

sys.taskInit(function()
    sys.wait(5000)
    --sdcard.init()
    pmd.ldoset(6,pmd.LDO_VMMC) -- 必须打开供电
    --fatfs.debug() -- 调试模式
    fatfs.init() -- 初始化, 默认连接SPI_1, 片选是GPIO3
    local fsm, err = fatfs.getfree() -- 真正初始化
    log.info("FatFs", fsm, err)
    if fsm ~= nil then
        log.info("FatFS","Total", tostring(fsm.total_kb) .. "kb", tostring(fsm.total_kb/1024) .. "mb")
        log.info("FatFS","Free", tostring(fsm.free_kb) .. "kb", tostring(fsm.free_kb/1024) .. "mb")
    else
        log.info("FatFS", "getfree", "error", err)
    end
    -- 枚举根目录
    log.info("FatFS","----------------------------------------------------")
    local re, files, msg = fatfs.lsdir("/")
    if re == 0 then
        --log.info("file count", #files)
        for k,v in pairs(files) do 
            log.info("FatFS", k, v.size, v.date, v.time, v.attrib, v.isdir)
        end
    else
        log.info("FatFS", re, msg)
    end
    -- 创建并写入文件
    local re, re2, re3, file
    log.info("FatFS","open lua.txt for write")
    file, re = fatfs.open("lua.txt", fatfs.FA_WRITE, fatfs.FA_CREATE_ALWAYS, 0)
    log.info("FatFS","open result", re, file)
    if file == nil then
        log.info("FatFS","open lua.txt for write, but fail", re)
    else
        re, re2, re3 = fatfs.write(file, "fatfs from wendal, IMEI=" .. misc.getImei())
        log.info("FatFS", "write", re, re2, re3)
        re, re2, re3 = fatfs.close(file)
        log.info("FatFS", "write", re, re2, re3)
    end
    -- 读取文件内容lua.txt
    log.info("FatFS","open lua.txt for read")
    local re, cnt, msg = fatfs.readfile("lua.txt")
    if cnt ~= nil then
        log.info("FatFS", "lua.txt", "content ---> ", cnt);
    else
        log.info("FatFS", "readfile", "fail", re);
    end
    -- 读取文件内容AIR720.TXT, 如果存在的话
    local re, cnt, msg = fatfs.readfile("AIR720.TXT", 30)
    if cnt ~= nil then
        log.info("FatFS", "AIR720.TXT", "content", cnt);
    else
        log.info("FatFS", "readfile", "fail", re);
    end

    log.info("FatFS","play mp3 from sdcard!!!")
    fatfs.playmp3("/mp3/sms.mp3")
    sys.wait(3000)
    --fatfs.playmp3("/mp3/citysky.mp3")
    --sys.wait(15000)
    fatfs.playmp3("/mp3/citysky2.mp3")
    sys.wait(5000)
    --log.info("FatFS","playmp3", re)
end)
--启动系统框架
sys.init(1, 0)
sys.run()
